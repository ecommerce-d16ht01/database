USE [master]
GO
/****** Object:  Database [CSDLQLTDKT_ChinhThuc]    Script Date: 5/11/2019 12:52:34 AM ******/
CREATE DATABASE [CSDLQLTDKT_ChinhThuc]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'CSDLQLTDKT_ChinhThuc', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\CSDLQLTDKT_ChinhThuc.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'CSDLQLTDKT_ChinhThuc_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\CSDLQLTDKT_ChinhThuc_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [CSDLQLTDKT_ChinhThuc].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET ARITHABORT OFF 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET  DISABLE_BROKER 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET  MULTI_USER 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET DB_CHAINING OFF 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET DELAYED_DURABILITY = DISABLED 
GO
USE [CSDLQLTDKT_ChinhThuc]
GO
/****** Object:  Table [dbo].[Table_BaiBao]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_BaiBao](
	[MaBaiBao] [int] IDENTITY(1,1) NOT NULL,
	[TenBaiBao] [nvarchar](255) NULL,
	[TenHoiNghi] [nvarchar](255) NULL,
	[NgayXuatBan] [date] NULL,
	[AnhMinhChung] [nvarchar](255) NULL,
	[SoDiem] [float] NULL,
	[KiemDuyet] [int] NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
	[NhanVien_MNV] [nvarchar](255) NULL,
	[NamHoc_MNH] [int] NULL,
	[NguoiKiemDuyet] [nvarchar](255) NULL,
 CONSTRAINT [PK_Table_BaiBao] PRIMARY KEY CLUSTERED 
(
	[MaBaiBao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_CapKhen]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_CapKhen](
	[MaCapKhen] [nvarchar](255) NOT NULL,
	[TenCapKhen] [nvarchar](255) NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
 CONSTRAINT [PK_Table_CapKhen] PRIMARY KEY CLUSTERED 
(
	[MaCapKhen] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_ChucVu]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_ChucVu](
	[MaChucVu] [nvarchar](255) NOT NULL,
	[TenChucVu] [nvarchar](255) NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
 CONSTRAINT [PK_Table_ChucVu] PRIMARY KEY CLUSTERED 
(
	[MaChucVu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_ChuyenNganh]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_ChuyenNganh](
	[MaChuyenNganh] [nvarchar](255) NOT NULL,
	[TenChuyenNganh] [nvarchar](255) NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
 CONSTRAINT [PK_Table_ChuyenNganh] PRIMARY KEY CLUSTERED 
(
	[MaChuyenNganh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_Comment]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_Comment](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[User_id] [nvarchar](255) NULL,
	[TinTuc_id] [nvarchar](255) NULL,
	[SuKien_id] [int] NULL,
	[NoiDung] [nvarchar](255) NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
	[TenNguoiComment] [nvarchar](255) NULL,
 CONSTRAINT [PK_Table_Comment] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_DanhHieu]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_DanhHieu](
	[MaDanhHieu] [nvarchar](255) NOT NULL,
	[TenDanhHieu] [nvarchar](255) NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
 CONSTRAINT [PK_Table_DanhHieu] PRIMARY KEY CLUSTERED 
(
	[MaDanhHieu] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_DemSoLanTangLuong]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_DemSoLanTangLuong](
	[NhanVien_MNV] [nvarchar](255) NOT NULL,
	[SoLanTangLuong] [int] NOT NULL,
 CONSTRAINT [PK_Table_DemSoLanTangLuong] PRIMARY KEY CLUSTERED 
(
	[NhanVien_MNV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_DeNghiTangLuong]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_DeNghiTangLuong](
	[MaDNTL] [int] IDENTITY(1,1) NOT NULL,
	[NoiDung] [nvarchar](255) NULL,
	[ThoiDiemTangLuong] [date] NULL,
	[ThoiDiemHetHan] [date] NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
	[KhenThuong_MKT] [nvarchar](255) NULL,
	[NhanVien_MNV] [nvarchar](255) NULL,
 CONSTRAINT [PK_Table_DeNghiTangLuong] PRIMARY KEY CLUSTERED 
(
	[MaDNTL] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_DeTaiNCKH]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_DeTaiNCKH](
	[MaDeTai] [int] IDENTITY(1,1) NOT NULL,
	[TenDeTai] [nvarchar](255) NULL,
	[NgayNghiemThu] [date] NULL,
	[KiemDuyet] [int] NULL,
	[AnhMinhChung] [nvarchar](255) NULL,
	[SoDiem] [float] NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
	[NhanVien_MNV] [nvarchar](255) NULL,
	[NamHoc_MNH] [int] NULL,
 CONSTRAINT [PK_Table_DeTaiNCKH] PRIMARY KEY CLUSTERED 
(
	[MaDeTai] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_DeXuatKhenThuong]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_DeXuatKhenThuong](
	[MaDeXuatKhenThuong] [int] IDENTITY(1,1) NOT NULL,
	[PhongBan_MPB] [nvarchar](255) NOT NULL,
	[NamHoc_MNH] [int] NOT NULL,
	[NhanVien_MNV] [nvarchar](255) NULL,
	[TenDanhHieu] [nvarchar](255) NULL,
	[created_at] [date] NULL,
 CONSTRAINT [PK_Table_DeXuatKhenThuong_1] PRIMARY KEY CLUSTERED 
(
	[MaDeXuatKhenThuong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_DiemDuNamTruoc]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_DiemDuNamTruoc](
	[MaNhanVien] [nvarchar](255) NOT NULL,
	[MaNamHoc] [int] NOT NULL,
	[DiemDuNamTruoc] [float] NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
 CONSTRAINT [PK_Table_DiemDuNamTruoc] PRIMARY KEY CLUSTERED 
(
	[MaNhanVien] ASC,
	[MaNamHoc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_GioCongTac]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_GioCongTac](
	[MaGioCongTac] [int] IDENTITY(1,1) NOT NULL,
	[MoTaCongViec] [ntext] NULL,
	[SoGioCongTac] [float] NULL,
	[AnhMinhChung] [nvarchar](255) NULL,
	[KiemDuyet] [int] NULL,
	[GhiChu] [ntext] NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
	[NhanVien_MNV] [nvarchar](255) NULL,
	[NamHoc_MNH] [int] NULL,
 CONSTRAINT [PK_Table_GioCongTac] PRIMARY KEY CLUSTERED 
(
	[MaGioCongTac] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_HinhThucKhenThuong]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_HinhThucKhenThuong](
	[MaHinhThucKhenThuong] [nvarchar](255) NOT NULL,
	[TenHinhThucKhenThuong] [nvarchar](255) NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
 CONSTRAINT [PK_Table_HinhThucKhenThuong] PRIMARY KEY CLUSTERED 
(
	[MaHinhThucKhenThuong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_KhenThuong]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_KhenThuong](
	[MaKhenThuong] [nvarchar](255) NOT NULL,
	[SoQuyetDinh] [nvarchar](255) NULL,
	[NoiDungKhenThuong] [nvarchar](255) NULL,
	[NgayKhenThuong] [date] NULL,
	[NguoiKy] [nvarchar](255) NULL,
	[ThoiDiem] [date] NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
	[HinhThucKhenThuong_MHT] [nvarchar](255) NULL,
	[DanhHieu_MDH] [nvarchar](255) NULL,
	[CapKhen_MCK] [nvarchar](255) NULL,
	[NhanVien_MNV] [nvarchar](255) NULL,
 CONSTRAINT [PK_Table_KhenThuong] PRIMARY KEY CLUSTERED 
(
	[MaKhenThuong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_KhenThuongTapThe]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_KhenThuongTapThe](
	[MaKhenThuong] [nvarchar](255) NOT NULL,
	[SoQuyetDinh] [nvarchar](255) NULL,
	[NoiDungKhenThuong] [nvarchar](255) NULL,
	[NgayKhenThuong] [date] NULL,
	[NguoiKy] [nvarchar](255) NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
	[HinhThucKhenThuong_MHT] [nvarchar](255) NULL,
	[DanhHieu_MDH] [nvarchar](255) NULL,
	[CapKhen_MCK] [nvarchar](255) NULL,
	[ThoiDiem_MTD] [int] NULL,
	[PhongBan_MPB] [nvarchar](255) NULL,
 CONSTRAINT [PK_Table_KhenThuongTapThe] PRIMARY KEY CLUSTERED 
(
	[MaKhenThuong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_LoaiHinh]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_LoaiHinh](
	[MaLoaiHinh] [nvarchar](255) NOT NULL,
	[TenLoaiHinh] [nvarchar](255) NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
 CONSTRAINT [PK_Table_LoaiHinh] PRIMARY KEY CLUSTERED 
(
	[MaLoaiHinh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_NamHoc]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_NamHoc](
	[MaNamHoc] [int] IDENTITY(1,1) NOT NULL,
	[TenNamHoc] [nvarchar](255) NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
 CONSTRAINT [PK_Table_NamHoc] PRIMARY KEY CLUSTERED 
(
	[MaNamHoc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_NhanVien]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_NhanVien](
	[MaNhanVien] [nvarchar](255) NOT NULL,
	[HoNhanVien] [nvarchar](255) NULL,
	[TenNhanVien] [nvarchar](255) NULL,
	[NgaySinh] [date] NULL,
	[GioiTinh] [bit] NULL,
	[DiaChi] [nvarchar](255) NULL,
	[SoCMND] [int] NULL,
	[SDT] [int] NULL,
	[Email] [nvarchar](255) NULL,
	[HinhAnh] [nvarchar](255) NULL,
	[NamVaoLam] [int] NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
	[ChucVu_MCV] [nvarchar](255) NULL,
	[PhongBan_MPB] [nvarchar](255) NULL,
	[TrinhDo_MTD] [nvarchar](255) NULL,
	[ChuyenNganh_MCN] [nvarchar](255) NULL,
 CONSTRAINT [PK_Table_NhanVien] PRIMARY KEY CLUSTERED 
(
	[MaNhanVien] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_PhongBan]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_PhongBan](
	[MaPhongBan] [nvarchar](255) NOT NULL,
	[TenPhongBan] [nvarchar](255) NULL,
	[HinhAnh] [nvarchar](255) NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
 CONSTRAINT [PK_Table_Khoa] PRIMARY KEY CLUSTERED 
(
	[MaPhongBan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_QuyetDinhTDKT]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_QuyetDinhTDKT](
	[MaQuyetDinh] [int] IDENTITY(1,1) NOT NULL,
	[TenQuyetDinh] [nvarchar](255) NULL,
	[Link] [nvarchar](255) NULL,
	[NgayBanHanh] [date] NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
 CONSTRAINT [PK_Table_QuyetDinhTDKT] PRIMARY KEY CLUSTERED 
(
	[MaQuyetDinh] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_SangKienKinhNghiem]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_SangKienKinhNghiem](
	[MaSKTN] [int] IDENTITY(1,1) NOT NULL,
	[TenSKTN] [nvarchar](255) NULL,
	[AnhMinhChung] [nvarchar](255) NULL,
	[SoDiem] [float] NULL,
	[KiemDuyet] [int] NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
	[NhanVien_MNV] [nvarchar](255) NULL,
	[NamHoc_MNH] [int] NULL,
 CONSTRAINT [PK_Table_SangKienKinhNghiem] PRIMARY KEY CLUSTERED 
(
	[MaSKTN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_SuKien]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_SuKien](
	[MaSuKien] [int] IDENTITY(1,1) NOT NULL,
	[TenSuKien] [nvarchar](255) NULL,
	[NoiDung] [nvarchar](max) NULL,
	[HinhAnh] [nvarchar](255) NULL,
	[SoLuotXem] [int] NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
 CONSTRAINT [PK_Table_SuKien] PRIMARY KEY CLUSTERED 
(
	[MaSuKien] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_TangLuong]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_TangLuong](
	[MaTangLuong] [int] IDENTITY(1,1) NOT NULL,
	[NoiDung] [nvarchar](255) NULL,
	[ThoiDiemTangLuong] [date] NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
	[NhanVien_MNV] [nvarchar](255) NULL,
	[KhenThuong_MKT] [nchar](10) NULL,
	[DeNghiTangLuong_MDNTL] [int] NULL,
 CONSTRAINT [PK_Table_TangLuong] PRIMARY KEY CLUSTERED 
(
	[MaTangLuong] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_ThoiDiem]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_ThoiDiem](
	[MaThoiDiem] [int] IDENTITY(1,1) NOT NULL,
	[ThoiDiemKhenThuong] [int] NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
 CONSTRAINT [PK_Table_ThoiDiem_1] PRIMARY KEY CLUSTERED 
(
	[MaThoiDiem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_ThongBao]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_ThongBao](
	[MaThongBao] [int] IDENTITY(1,1) NOT NULL,
	[NoiDung] [nvarchar](255) NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
 CONSTRAINT [PK_Table_ThongBao] PRIMARY KEY CLUSTERED 
(
	[MaThongBao] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_TinTuc]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_TinTuc](
	[MaTinTuc] [nvarchar](255) NOT NULL,
	[TenTinTuc] [nvarchar](255) NULL,
	[NoiDung] [nvarchar](max) NULL,
	[HinhAnh] [nvarchar](255) NULL,
	[NoiBat] [int] NULL,
	[SoLuotXem] [int] NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
 CONSTRAINT [PK_Table_TinTuc] PRIMARY KEY CLUSTERED 
(
	[MaTinTuc] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_TrinhDoHocVan]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_TrinhDoHocVan](
	[MaTDHV] [nvarchar](255) NOT NULL,
	[TenTDHV] [nvarchar](255) NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
 CONSTRAINT [PK_Table_TrinhDoHocVan] PRIMARY KEY CLUSTERED 
(
	[MaTDHV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_User]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_User](
	[MaUser] [nvarchar](255) NOT NULL,
	[TaiKhoan] [nvarchar](255) NULL,
	[MatKhau] [nvarchar](255) NULL,
	[CapDo] [int] NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
	[NhanVien_MNV] [nvarchar](255) NULL,
 CONSTRAINT [PK_Table_User] PRIMARY KEY CLUSTERED 
(
	[MaUser] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Table_VanBanTDKT]    Script Date: 5/11/2019 12:52:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Table_VanBanTDKT](
	[MaVanBan] [int] IDENTITY(1,1) NOT NULL,
	[TenVanBan] [nvarchar](255) NULL,
	[Link] [nvarchar](255) NULL,
	[NgayBanHanh] [date] NULL,
	[created_at] [date] NULL,
	[updated_at] [date] NULL,
	[LoaiHinh_MLH] [nvarchar](255) NULL,
 CONSTRAINT [PK_Table_VanBanTDKT] PRIMARY KEY CLUSTERED 
(
	[MaVanBan] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Table_BaiBao] ON 

INSERT [dbo].[Table_BaiBao] ([MaBaiBao], [TenBaiBao], [TenHoiNghi], [NgayXuatBan], [AnhMinhChung], [SoDiem], [KiemDuyet], [created_at], [updated_at], [NhanVien_MNV], [NamHoc_MNH], [NguoiKiemDuyet]) VALUES (63, N'Bài Báo Đề Cử Cấp Khoa', N'Cấp Khoa', CAST(N'2019-04-29' AS Date), N'detainckh10052019.PNG', 2, 1, CAST(N'2019-04-29' AS Date), NULL, N'NV004', 5, NULL)
INSERT [dbo].[Table_BaiBao] ([MaBaiBao], [TenBaiBao], [TenHoiNghi], [NgayXuatBan], [AnhMinhChung], [SoDiem], [KiemDuyet], [created_at], [updated_at], [NhanVien_MNV], [NamHoc_MNH], [NguoiKiemDuyet]) VALUES (64, N'Bài Báo Bình Thường', N'TDMU', CAST(N'2019-04-29' AS Date), N'detainckh10052019.PNG', 1.5, 1, CAST(N'2019-04-29' AS Date), NULL, N'NV003', 5, NULL)
INSERT [dbo].[Table_BaiBao] ([MaBaiBao], [TenBaiBao], [TenHoiNghi], [NgayXuatBan], [AnhMinhChung], [SoDiem], [KiemDuyet], [created_at], [updated_at], [NhanVien_MNV], [NamHoc_MNH], [NguoiKiemDuyet]) VALUES (66, N'Bài Báo Demo', N'Cấp Trường', CAST(N'2019-04-29' AS Date), N'detainckh10052019.PNG', 2, 1, CAST(N'2019-04-29' AS Date), NULL, N'NV002', 5, NULL)
INSERT [dbo].[Table_BaiBao] ([MaBaiBao], [TenBaiBao], [TenHoiNghi], [NgayXuatBan], [AnhMinhChung], [SoDiem], [KiemDuyet], [created_at], [updated_at], [NhanVien_MNV], [NamHoc_MNH], [NguoiKiemDuyet]) VALUES (67, N'Demo', N'Cấp Khoa', CAST(N'2019-04-29' AS Date), N'detainckh10052019.PNG', 1.5, 1, CAST(N'2019-04-29' AS Date), NULL, N'NV005', 5, NULL)
INSERT [dbo].[Table_BaiBao] ([MaBaiBao], [TenBaiBao], [TenHoiNghi], [NgayXuatBan], [AnhMinhChung], [SoDiem], [KiemDuyet], [created_at], [updated_at], [NhanVien_MNV], [NamHoc_MNH], [NguoiKiemDuyet]) VALUES (68, N'Bài Báo Đề Cử Cấp Khoa 4', N'Cấp Khoa', CAST(N'2019-05-10' AS Date), N'detainckh10052019.PNG', 0.5, 1, CAST(N'2019-05-10' AS Date), NULL, N'NV003', 5, N'AVE')
INSERT [dbo].[Table_BaiBao] ([MaBaiBao], [TenBaiBao], [TenHoiNghi], [NgayXuatBan], [AnhMinhChung], [SoDiem], [KiemDuyet], [created_at], [updated_at], [NhanVien_MNV], [NamHoc_MNH], [NguoiKiemDuyet]) VALUES (69, N'Bài Báo Đề Xuất Khen Thưởng 2', N'Cấp Khoa', CAST(N'2017-06-13' AS Date), N'detainckh11052019.PNG', 2, 1, CAST(N'2019-05-11' AS Date), NULL, N'NV003', 4, N'AVE')
INSERT [dbo].[Table_BaiBao] ([MaBaiBao], [TenBaiBao], [TenHoiNghi], [NgayXuatBan], [AnhMinhChung], [SoDiem], [KiemDuyet], [created_at], [updated_at], [NhanVien_MNV], [NamHoc_MNH], [NguoiKiemDuyet]) VALUES (70, N'Bài Báo Đề Cử Cấp Khoa 3', N'Cấp Khoa', CAST(N'2017-01-10' AS Date), N'detainckh11052019.PNG', 2, 1, CAST(N'2019-05-11' AS Date), NULL, N'NV003', 3, N'AVE')
INSERT [dbo].[Table_BaiBao] ([MaBaiBao], [TenBaiBao], [TenHoiNghi], [NgayXuatBan], [AnhMinhChung], [SoDiem], [KiemDuyet], [created_at], [updated_at], [NhanVien_MNV], [NamHoc_MNH], [NguoiKiemDuyet]) VALUES (72, N'Bài Báo Đề Cử Cấp Khoa 5', N'Cấp Khoa', CAST(N'2018-01-01' AS Date), N'detainckh11052019.PNG', 1.25, NULL, CAST(N'2019-05-11' AS Date), NULL, N'NV003', 3, NULL)
SET IDENTITY_INSERT [dbo].[Table_BaiBao] OFF
INSERT [dbo].[Table_CapKhen] ([MaCapKhen], [TenCapKhen], [created_at], [updated_at]) VALUES (N'CK01', N'Tỉnh', CAST(N'2018-11-12' AS Date), NULL)
INSERT [dbo].[Table_CapKhen] ([MaCapKhen], [TenCapKhen], [created_at], [updated_at]) VALUES (N'Ck02', N'Huyện', NULL, NULL)
INSERT [dbo].[Table_ChucVu] ([MaChucVu], [TenChucVu], [created_at], [updated_at]) VALUES (N'CV06', N'Phó Phòng', CAST(N'2018-11-09' AS Date), NULL)
INSERT [dbo].[Table_ChucVu] ([MaChucVu], [TenChucVu], [created_at], [updated_at]) VALUES (N'CV07', N'Giảng Viên', CAST(N'2018-11-09' AS Date), NULL)
INSERT [dbo].[Table_ChucVu] ([MaChucVu], [TenChucVu], [created_at], [updated_at]) VALUES (N'CV08', N'Sinh Viên', CAST(N'2018-11-09' AS Date), NULL)
INSERT [dbo].[Table_ChucVu] ([MaChucVu], [TenChucVu], [created_at], [updated_at]) VALUES (N'CV09', N'Trưởng Bộ Môn', CAST(N'2018-11-09' AS Date), NULL)
INSERT [dbo].[Table_ChucVu] ([MaChucVu], [TenChucVu], [created_at], [updated_at]) VALUES (N'CV10', N'Phó Bộ Môn', CAST(N'2018-11-09' AS Date), NULL)
INSERT [dbo].[Table_ChuyenNganh] ([MaChuyenNganh], [TenChuyenNganh], [created_at], [updated_at]) VALUES (N'CN01', N'Hệ Thống Thông Tin', CAST(N'2018-11-10' AS Date), NULL)
SET IDENTITY_INSERT [dbo].[Table_Comment] ON 

INSERT [dbo].[Table_Comment] ([id], [User_id], [TinTuc_id], [SuKien_id], [NoiDung], [created_at], [updated_at], [TenNguoiComment]) VALUES (11, NULL, N'TT009', NULL, N'demo', CAST(N'2019-04-09' AS Date), NULL, N'Thành')
INSERT [dbo].[Table_Comment] ([id], [User_id], [TinTuc_id], [SuKien_id], [NoiDung], [created_at], [updated_at], [TenNguoiComment]) VALUES (12, NULL, N'TT009', NULL, N'demo2
', CAST(N'2019-04-14' AS Date), NULL, N'Thành')
SET IDENTITY_INSERT [dbo].[Table_Comment] OFF
INSERT [dbo].[Table_DanhHieu] ([MaDanhHieu], [TenDanhHieu], [created_at], [updated_at]) VALUES (N'DH01', N'Chiến Sĩ Toàn Quốc', CAST(N'2018-11-10' AS Date), CAST(N'2018-12-27' AS Date))
INSERT [dbo].[Table_DanhHieu] ([MaDanhHieu], [TenDanhHieu], [created_at], [updated_at]) VALUES (N'DH02', N'Chiến sĩ thi đua cấp bộ, ngành, tỉnh, đoàn thể trung ương', CAST(N'2018-12-27' AS Date), NULL)
INSERT [dbo].[Table_DanhHieu] ([MaDanhHieu], [TenDanhHieu], [created_at], [updated_at]) VALUES (N'DH03', N'Chiến sĩ thi đua cơ sở', CAST(N'2018-12-27' AS Date), NULL)
INSERT [dbo].[Table_DanhHieu] ([MaDanhHieu], [TenDanhHieu], [created_at], [updated_at]) VALUES (N'DH04', N'Lao động tiên tiến, Chiến sĩ tiên tiến', CAST(N'2018-12-27' AS Date), NULL)
INSERT [dbo].[Table_DanhHieu] ([MaDanhHieu], [TenDanhHieu], [created_at], [updated_at]) VALUES (N'DHTT01', N'Cờ thi đua Chính phủ', CAST(N'2018-12-27' AS Date), NULL)
INSERT [dbo].[Table_DanhHieu] ([MaDanhHieu], [TenDanhHieu], [created_at], [updated_at]) VALUES (N'DHTT02', N'Cờ thi đua cấp bộ, ngành, tỉnh, đoàn thể trung ương', CAST(N'2018-12-27' AS Date), NULL)
INSERT [dbo].[Table_DanhHieu] ([MaDanhHieu], [TenDanhHieu], [created_at], [updated_at]) VALUES (N'DHTT03', N'Tập thể Lao động xuất sắc, Đơn vị quyết thắng', CAST(N'2018-12-27' AS Date), NULL)
INSERT [dbo].[Table_DanhHieu] ([MaDanhHieu], [TenDanhHieu], [created_at], [updated_at]) VALUES (N'DHTT04', N'Tập thể Lao động tiên tiến, Chiến sĩ tiên tiến', CAST(N'2018-12-27' AS Date), NULL)
INSERT [dbo].[Table_DemSoLanTangLuong] ([NhanVien_MNV], [SoLanTangLuong]) VALUES (N'NV001', 9)
INSERT [dbo].[Table_DemSoLanTangLuong] ([NhanVien_MNV], [SoLanTangLuong]) VALUES (N'NV002', 4)
INSERT [dbo].[Table_DemSoLanTangLuong] ([NhanVien_MNV], [SoLanTangLuong]) VALUES (N'NV003', 10)
INSERT [dbo].[Table_DemSoLanTangLuong] ([NhanVien_MNV], [SoLanTangLuong]) VALUES (N'NV004', 2)
INSERT [dbo].[Table_DemSoLanTangLuong] ([NhanVien_MNV], [SoLanTangLuong]) VALUES (N'NV005', 3)
SET IDENTITY_INSERT [dbo].[Table_DeNghiTangLuong] ON 

INSERT [dbo].[Table_DeNghiTangLuong] ([MaDNTL], [NoiDung], [ThoiDiemTangLuong], [ThoiDiemHetHan], [created_at], [updated_at], [KhenThuong_MKT], [NhanVien_MNV]) VALUES (3021, N'Đề Nghị Tăng Lương 9 Tháng', CAST(N'2018-01-01' AS Date), CAST(N'2018-09-30' AS Date), CAST(N'2018-01-01' AS Date), NULL, NULL, N'NV002')
INSERT [dbo].[Table_DeNghiTangLuong] ([MaDNTL], [NoiDung], [ThoiDiemTangLuong], [ThoiDiemHetHan], [created_at], [updated_at], [KhenThuong_MKT], [NhanVien_MNV]) VALUES (3025, N'Đề Nghị Tăng Lương 6 Tháng', CAST(N'2018-01-01' AS Date), CAST(N'2018-06-30' AS Date), CAST(N'2018-01-01' AS Date), NULL, NULL, N'NV004')
INSERT [dbo].[Table_DeNghiTangLuong] ([MaDNTL], [NoiDung], [ThoiDiemTangLuong], [ThoiDiemHetHan], [created_at], [updated_at], [KhenThuong_MKT], [NhanVien_MNV]) VALUES (3026, N'Đề Nghị Tăng Lương 6 Tháng', CAST(N'2019-01-01' AS Date), CAST(N'2019-06-30' AS Date), CAST(N'2019-01-01' AS Date), NULL, NULL, N'NV005')
INSERT [dbo].[Table_DeNghiTangLuong] ([MaDNTL], [NoiDung], [ThoiDiemTangLuong], [ThoiDiemHetHan], [created_at], [updated_at], [KhenThuong_MKT], [NhanVien_MNV]) VALUES (3029, N'Đề Nghị Tăng Lương 9 Tháng', CAST(N'2017-01-01' AS Date), CAST(N'2017-09-30' AS Date), CAST(N'2017-01-01' AS Date), NULL, NULL, N'NV001')
INSERT [dbo].[Table_DeNghiTangLuong] ([MaDNTL], [NoiDung], [ThoiDiemTangLuong], [ThoiDiemHetHan], [created_at], [updated_at], [KhenThuong_MKT], [NhanVien_MNV]) VALUES (3033, N'Đề Nghị Tăng Lương 9 Tháng', CAST(N'2019-01-01' AS Date), CAST(N'2019-09-30' AS Date), CAST(N'2019-01-01' AS Date), NULL, NULL, N'NV003')
SET IDENTITY_INSERT [dbo].[Table_DeNghiTangLuong] OFF
SET IDENTITY_INSERT [dbo].[Table_DeTaiNCKH] ON 

INSERT [dbo].[Table_DeTaiNCKH] ([MaDeTai], [TenDeTai], [NgayNghiemThu], [KiemDuyet], [AnhMinhChung], [SoDiem], [created_at], [updated_at], [NhanVien_MNV], [NamHoc_MNH]) VALUES (16, N'Đề tài NCKH hỗ trợ Quản Lý Khen Thưởng', CAST(N'2019-05-10' AS Date), 1, N'detainckh10052019.PNG', 0.25, CAST(N'2019-05-10' AS Date), NULL, N'NV003', 5)
SET IDENTITY_INSERT [dbo].[Table_DeTaiNCKH] OFF
SET IDENTITY_INSERT [dbo].[Table_DeXuatKhenThuong] ON 

INSERT [dbo].[Table_DeXuatKhenThuong] ([MaDeXuatKhenThuong], [PhongBan_MPB], [NamHoc_MNH], [NhanVien_MNV], [TenDanhHieu], [created_at]) VALUES (10, N'PB02', 5, N'NV004', N'Lao động tiên tiến', CAST(N'2019-04-29' AS Date))
INSERT [dbo].[Table_DeXuatKhenThuong] ([MaDeXuatKhenThuong], [PhongBan_MPB], [NamHoc_MNH], [NhanVien_MNV], [TenDanhHieu], [created_at]) VALUES (11, N'PB02', 5, N'NV003', N'Lao động tiên tiến', CAST(N'2019-04-29' AS Date))
INSERT [dbo].[Table_DeXuatKhenThuong] ([MaDeXuatKhenThuong], [PhongBan_MPB], [NamHoc_MNH], [NhanVien_MNV], [TenDanhHieu], [created_at]) VALUES (12, N'PB01', 5, N'NV002', N'Chiến sĩ thi đua cơ sở', CAST(N'2019-04-29' AS Date))
INSERT [dbo].[Table_DeXuatKhenThuong] ([MaDeXuatKhenThuong], [PhongBan_MPB], [NamHoc_MNH], [NhanVien_MNV], [TenDanhHieu], [created_at]) VALUES (13, N'PB01', 5, N'NV005', N'Lao động tiên tiến', CAST(N'2019-04-29' AS Date))
SET IDENTITY_INSERT [dbo].[Table_DeXuatKhenThuong] OFF
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV001', 1, 0, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV001', 2, 0, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV001', 3, 0, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV001', 4, 0, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV001', 5, 0, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV002', 1, 0, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV002', 2, 0, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV002', 3, 0, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV002', 4, 0, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV002', 5, 0, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV003', 1, 0, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV003', 2, 0, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV003', 3, 1.875, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV003', 4, 3.75, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV003', 5, 0, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV004', 1, 0, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV004', 2, 0, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV004', 3, 0, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV004', 4, 0, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV004', 5, 0, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV005', 1, 0, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV005', 3, 0, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV005', 4, 0, NULL, NULL)
INSERT [dbo].[Table_DiemDuNamTruoc] ([MaNhanVien], [MaNamHoc], [DiemDuNamTruoc], [created_at], [updated_at]) VALUES (N'NV005', 5, 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Table_GioCongTac] ON 

INSERT [dbo].[Table_GioCongTac] ([MaGioCongTac], [MoTaCongViec], [SoGioCongTac], [AnhMinhChung], [KiemDuyet], [GhiChu], [created_at], [updated_at], [NhanVien_MNV], [NamHoc_MNH]) VALUES (12, N'Thực Hành Nghiên Cứu Bài Báo', 50, N'ThoiKhoaBieu29042019.PNG', 1, N'Demo', CAST(N'2019-04-29' AS Date), NULL, N'NV003', 4)
INSERT [dbo].[Table_GioCongTac] ([MaGioCongTac], [MoTaCongViec], [SoGioCongTac], [AnhMinhChung], [KiemDuyet], [GhiChu], [created_at], [updated_at], [NhanVien_MNV], [NamHoc_MNH]) VALUES (13, N'Thực Hành Nghiên Cứu Bài Báo 2', 80, N'ThoiKhoaBieu29042019.PNG', 1, N'Demo 2', CAST(N'2019-04-29' AS Date), NULL, N'NV003', 4)
INSERT [dbo].[Table_GioCongTac] ([MaGioCongTac], [MoTaCongViec], [SoGioCongTac], [AnhMinhChung], [KiemDuyet], [GhiChu], [created_at], [updated_at], [NhanVien_MNV], [NamHoc_MNH]) VALUES (14, N'Thực Hành Nghiên Cứu Bài Báo 3', 150, N'ThoiKhoaBieu29042019.PNG', 1, N'Demo 23', CAST(N'2019-04-29' AS Date), NULL, N'NV002', 5)
INSERT [dbo].[Table_GioCongTac] ([MaGioCongTac], [MoTaCongViec], [SoGioCongTac], [AnhMinhChung], [KiemDuyet], [GhiChu], [created_at], [updated_at], [NhanVien_MNV], [NamHoc_MNH]) VALUES (15, N'ABC', 8, N'ThoiKhoaBieu29042019.PNG', 1, N'Demo', CAST(N'2019-04-29' AS Date), NULL, N'NV005', 5)
INSERT [dbo].[Table_GioCongTac] ([MaGioCongTac], [MoTaCongViec], [SoGioCongTac], [AnhMinhChung], [KiemDuyet], [GhiChu], [created_at], [updated_at], [NhanVien_MNV], [NamHoc_MNH]) VALUES (16, N'ABCD', 115, N'ThoiKhoaBieu29042019.PNG', 1, N'Demo 2', CAST(N'2019-04-29' AS Date), NULL, N'NV005', 5)
INSERT [dbo].[Table_GioCongTac] ([MaGioCongTac], [MoTaCongViec], [SoGioCongTac], [AnhMinhChung], [KiemDuyet], [GhiChu], [created_at], [updated_at], [NhanVien_MNV], [NamHoc_MNH]) VALUES (17, N'Thực Hành Nghiên Cứu Bài Báo 8', 80, N'detainckh10052019.PNG', 1, N'Demo 213', CAST(N'2019-05-10' AS Date), NULL, N'NV003', 5)
SET IDENTITY_INSERT [dbo].[Table_GioCongTac] OFF
INSERT [dbo].[Table_HinhThucKhenThuong] ([MaHinhThucKhenThuong], [TenHinhThucKhenThuong], [created_at], [updated_at]) VALUES (N'HT01', N'Huân chương', CAST(N'2018-12-27' AS Date), NULL)
INSERT [dbo].[Table_HinhThucKhenThuong] ([MaHinhThucKhenThuong], [TenHinhThucKhenThuong], [created_at], [updated_at]) VALUES (N'HT02', N'Huy chương', CAST(N'2018-12-27' AS Date), NULL)
INSERT [dbo].[Table_HinhThucKhenThuong] ([MaHinhThucKhenThuong], [TenHinhThucKhenThuong], [created_at], [updated_at]) VALUES (N'HT03', N'Danh hiệu vinh dự nhà nước', CAST(N'2018-12-27' AS Date), NULL)
INSERT [dbo].[Table_HinhThucKhenThuong] ([MaHinhThucKhenThuong], [TenHinhThucKhenThuong], [created_at], [updated_at]) VALUES (N'HT04', N'Giải thưởng Hồ Chí Minh, Giải thưởng nhà nước', CAST(N'2018-12-27' AS Date), NULL)
INSERT [dbo].[Table_HinhThucKhenThuong] ([MaHinhThucKhenThuong], [TenHinhThucKhenThuong], [created_at], [updated_at]) VALUES (N'HT05', N'Kỷ niệm chương, Huy hiệu', CAST(N'2018-12-27' AS Date), NULL)
INSERT [dbo].[Table_HinhThucKhenThuong] ([MaHinhThucKhenThuong], [TenHinhThucKhenThuong], [created_at], [updated_at]) VALUES (N'HT06', N'Bằng khen', CAST(N'2018-12-27' AS Date), NULL)
INSERT [dbo].[Table_HinhThucKhenThuong] ([MaHinhThucKhenThuong], [TenHinhThucKhenThuong], [created_at], [updated_at]) VALUES (N'HT07', N'Giấy khen', CAST(N'2018-12-27' AS Date), NULL)
INSERT [dbo].[Table_KhenThuong] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [ThoiDiem], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [NhanVien_MNV]) VALUES (N'KT001', N'QĐ125', N'Phấn đầu trong thi đua', NULL, N'ACE', CAST(N'2017-03-12' AS Date), CAST(N'2019-03-12' AS Date), NULL, N'HT06', N'DH04', N'CK01', N'NV003')
INSERT [dbo].[Table_KhenThuong] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [ThoiDiem], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [NhanVien_MNV]) VALUES (N'KT002', N'QĐ125', N'Phấn đầu trong thi đua', NULL, N'ACE', CAST(N'2018-03-12' AS Date), CAST(N'2019-03-12' AS Date), NULL, N'HT06', N'DH04', N'Ck02', N'NV003')
INSERT [dbo].[Table_KhenThuong] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [ThoiDiem], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [NhanVien_MNV]) VALUES (N'KT003', N'VD', N'Xuất Sắc', NULL, N'ACE', CAST(N'2019-01-01' AS Date), CAST(N'2019-04-16' AS Date), NULL, N'HT06', N'DH03', N'CK01', N'NV003')
INSERT [dbo].[Table_KhenThuong] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [ThoiDiem], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [NhanVien_MNV]) VALUES (N'KTNV001-001', N'QĐ125', N'Phấn đấu trong thi đua', NULL, N'ACE', CAST(N'2015-03-12' AS Date), CAST(N'2019-03-13' AS Date), NULL, N'HT06', N'DH04', N'CK01', N'NV001')
INSERT [dbo].[Table_KhenThuong] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [ThoiDiem], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [NhanVien_MNV]) VALUES (N'KTNV001-002', N'QĐ125', N'Phấn đấu trong thi đua', NULL, N'ACE', CAST(N'2016-03-12' AS Date), CAST(N'2019-03-13' AS Date), NULL, N'HT06', N'DH04', N'CK01', N'NV001')
INSERT [dbo].[Table_KhenThuong] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [ThoiDiem], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [NhanVien_MNV]) VALUES (N'KTNV001-003', N'QĐ125', N'Phấn đấu trong thi đua', NULL, N'ACE', CAST(N'2017-03-12' AS Date), CAST(N'2019-03-13' AS Date), CAST(N'2019-03-23' AS Date), N'HT06', N'DH03', N'CK01', N'NV001')
INSERT [dbo].[Table_KhenThuong] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [ThoiDiem], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [NhanVien_MNV]) VALUES (N'KTNV002-001', N'QĐ125', N'Phấn đấu trong thi đua', NULL, N'ACE', CAST(N'2016-03-12' AS Date), CAST(N'2019-03-13' AS Date), NULL, N'HT06', N'DH04', N'CK01', N'NV002')
INSERT [dbo].[Table_KhenThuong] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [ThoiDiem], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [NhanVien_MNV]) VALUES (N'KTNV002-003', N'QĐ125', N'Phấn đấu trong thi đua', NULL, N'ACE', CAST(N'2017-03-12' AS Date), CAST(N'2019-03-13' AS Date), NULL, N'HT06', N'DH04', N'CK01', N'NV002')
INSERT [dbo].[Table_KhenThuong] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [ThoiDiem], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [NhanVien_MNV]) VALUES (N'KTNV003-002', N'QĐ125', N'Phấn đấu trong thi đua', NULL, N'ACE', CAST(N'2018-03-12' AS Date), CAST(N'2019-03-13' AS Date), NULL, N'HT06', N'DH03', N'CK01', N'NV002')
INSERT [dbo].[Table_KhenThuong] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [ThoiDiem], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [NhanVien_MNV]) VALUES (N'KTNV004-001', N'QĐ125', N'Phấn đấu trong thi đua', NULL, N'ACE', CAST(N'2016-03-12' AS Date), CAST(N'2019-03-13' AS Date), NULL, N'HT06', N'DH04', N'CK01', N'NV004')
INSERT [dbo].[Table_KhenThuong] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [ThoiDiem], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [NhanVien_MNV]) VALUES (N'KTNV004-002', N'QĐ125', N'Phấn đấu trong thi đua', NULL, N'ACE', CAST(N'2017-03-12' AS Date), CAST(N'2019-03-13' AS Date), NULL, N'HT06', N'DH04', N'CK01', N'NV004')
INSERT [dbo].[Table_KhenThuong] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [ThoiDiem], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [NhanVien_MNV]) VALUES (N'KTNV004-003', N'QĐ125', N'Phấn đấu trong thi đua', NULL, N'ACE', CAST(N'2019-03-12' AS Date), CAST(N'2019-03-13' AS Date), NULL, N'HT06', N'DH04', N'CK01', N'NV004')
INSERT [dbo].[Table_KhenThuong] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [ThoiDiem], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [NhanVien_MNV]) VALUES (N'KTNV004-004', N'123/5A', N'Xuất sắc trong những năm gần đây', NULL, N'ACE', CAST(N'2018-04-18' AS Date), CAST(N'2019-04-06' AS Date), NULL, N'HT06', N'DH04', N'CK01', N'NV004')
INSERT [dbo].[Table_KhenThuong] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [ThoiDiem], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [NhanVien_MNV]) VALUES (N'KTNV005-001', N'132/5', N'Xuất sắc trong những năm gần đây', NULL, N'ACE', CAST(N'2019-04-08' AS Date), CAST(N'2019-04-07' AS Date), NULL, N'HT02', N'DH04', N'CK01', N'NV005')
INSERT [dbo].[Table_KhenThuong] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [ThoiDiem], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [NhanVien_MNV]) VALUES (N'KTNV005-002', N'132/5', N'Xuất sắc trong những năm gần đây', NULL, N'ACE', CAST(N'2017-04-20' AS Date), CAST(N'2019-04-07' AS Date), NULL, N'HT04', N'DH04', N'Ck02', N'NV005')
INSERT [dbo].[Table_KhenThuong] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [ThoiDiem], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [NhanVien_MNV]) VALUES (N'KTNV005-003', N'132/5', N'Xuất sắc trong những năm gần đây', NULL, N'ACE', CAST(N'2018-04-18' AS Date), CAST(N'2019-04-07' AS Date), NULL, N'HT03', N'DH04', N'CK01', N'NV005')
INSERT [dbo].[Table_KhenThuongTapThe] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [ThoiDiem_MTD], [PhongBan_MPB]) VALUES (N'asd', N'qwe', N'ds', NULL, N'asd', CAST(N'2018-11-27' AS Date), CAST(N'2019-04-24' AS Date), N'HT03', N'DH01', N'CK01', NULL, N'PB01')
INSERT [dbo].[Table_KhenThuongTapThe] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [ThoiDiem_MTD], [PhongBan_MPB]) VALUES (N'asd222', N'asd', N'sad', NULL, N'asdd', CAST(N'2018-11-27' AS Date), NULL, NULL, N'DH01', N'CK01', NULL, N'PB01')
INSERT [dbo].[Table_KhenThuongTapThe] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [ThoiDiem_MTD], [PhongBan_MPB]) VALUES (N'd11', N'asas', N'sad', NULL, N'sad', CAST(N'2018-11-27' AS Date), NULL, NULL, N'DH01', N'CK01', NULL, N'PB01')
INSERT [dbo].[Table_KhenThuongTapThe] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [ThoiDiem_MTD], [PhongBan_MPB]) VALUES (N'e1', N'asas', N'sad', NULL, N'Nguyễn Tấn Thành', CAST(N'2018-11-27' AS Date), NULL, NULL, N'DH01', N'CK01', NULL, N'PB01')
INSERT [dbo].[Table_KhenThuongTapThe] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [ThoiDiem_MTD], [PhongBan_MPB]) VALUES (N'KTPB01', N'123abc', N'So Good', NULL, N'ACE', CAST(N'2018-11-16' AS Date), NULL, NULL, N'DH01', N'CK01', NULL, N'PB01')
INSERT [dbo].[Table_KhenThuongTapThe] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [ThoiDiem_MTD], [PhongBan_MPB]) VALUES (N'sda2', N'wqe', N'as', NULL, N'asdd', CAST(N'2018-11-27' AS Date), NULL, NULL, N'DH01', N'CK01', NULL, N'PB01')
INSERT [dbo].[Table_KhenThuongTapThe] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [ThoiDiem_MTD], [PhongBan_MPB]) VALUES (N'ss', N'asd', N'asd', NULL, N'asdd', CAST(N'2018-11-27' AS Date), NULL, NULL, N'DH01', N'Ck02', NULL, N'PB01')
INSERT [dbo].[Table_KhenThuongTapThe] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [ThoiDiem_MTD], [PhongBan_MPB]) VALUES (N'xsa', N'asd', N'asd', NULL, N'asd', CAST(N'2018-11-27' AS Date), NULL, NULL, N'DH01', N'Ck02', NULL, N'PB01')
INSERT [dbo].[Table_KhenThuongTapThe] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [ThoiDiem_MTD], [PhongBan_MPB]) VALUES (N'xx11', N'asas', N'sad', NULL, N'sad', CAST(N'2018-11-27' AS Date), NULL, NULL, N'DH01', N'Ck02', NULL, N'PB01')
INSERT [dbo].[Table_KhenThuongTapThe] ([MaKhenThuong], [SoQuyetDinh], [NoiDungKhenThuong], [NgayKhenThuong], [NguoiKy], [created_at], [updated_at], [HinhThucKhenThuong_MHT], [DanhHieu_MDH], [CapKhen_MCK], [ThoiDiem_MTD], [PhongBan_MPB]) VALUES (N'xx222', N'sad', N'sad', NULL, N'sad', CAST(N'2018-11-27' AS Date), NULL, NULL, N'DH01', N'Ck02', NULL, N'PB01')
INSERT [dbo].[Table_LoaiHinh] ([MaLoaiHinh], [TenLoaiHinh], [created_at], [updated_at]) VALUES (N'LH01', N'Thông Báo', CAST(N'2019-02-04' AS Date), NULL)
INSERT [dbo].[Table_LoaiHinh] ([MaLoaiHinh], [TenLoaiHinh], [created_at], [updated_at]) VALUES (N'LH02', N'Chương Trình', CAST(N'2019-02-04' AS Date), NULL)
INSERT [dbo].[Table_LoaiHinh] ([MaLoaiHinh], [TenLoaiHinh], [created_at], [updated_at]) VALUES (N'LH03', N'Biểu Mẫu', CAST(N'2019-02-04' AS Date), NULL)
INSERT [dbo].[Table_LoaiHinh] ([MaLoaiHinh], [TenLoaiHinh], [created_at], [updated_at]) VALUES (N'LH04', N'Chỉ Thị', CAST(N'2019-02-04' AS Date), NULL)
INSERT [dbo].[Table_LoaiHinh] ([MaLoaiHinh], [TenLoaiHinh], [created_at], [updated_at]) VALUES (N'LH05', N'Nghị Định', CAST(N'2019-02-04' AS Date), NULL)
INSERT [dbo].[Table_LoaiHinh] ([MaLoaiHinh], [TenLoaiHinh], [created_at], [updated_at]) VALUES (N'LH06', N'Công Văn', CAST(N'2019-02-04' AS Date), NULL)
INSERT [dbo].[Table_LoaiHinh] ([MaLoaiHinh], [TenLoaiHinh], [created_at], [updated_at]) VALUES (N'LH07', N'Thông Tư', CAST(N'2019-02-04' AS Date), NULL)
INSERT [dbo].[Table_LoaiHinh] ([MaLoaiHinh], [TenLoaiHinh], [created_at], [updated_at]) VALUES (N'LH08', N'Hướng Dẫn', CAST(N'2019-02-04' AS Date), CAST(N'2019-02-04' AS Date))
SET IDENTITY_INSERT [dbo].[Table_NamHoc] ON 

INSERT [dbo].[Table_NamHoc] ([MaNamHoc], [TenNamHoc], [created_at], [updated_at]) VALUES (1, N'2015 - 2016', CAST(N'2019-02-01' AS Date), CAST(N'2019-02-01' AS Date))
INSERT [dbo].[Table_NamHoc] ([MaNamHoc], [TenNamHoc], [created_at], [updated_at]) VALUES (2, N'2016 - 2017', CAST(N'2019-02-01' AS Date), NULL)
INSERT [dbo].[Table_NamHoc] ([MaNamHoc], [TenNamHoc], [created_at], [updated_at]) VALUES (3, N'2017 - 2018', CAST(N'2019-02-01' AS Date), NULL)
INSERT [dbo].[Table_NamHoc] ([MaNamHoc], [TenNamHoc], [created_at], [updated_at]) VALUES (4, N'2018 - 2019', CAST(N'2019-02-01' AS Date), NULL)
INSERT [dbo].[Table_NamHoc] ([MaNamHoc], [TenNamHoc], [created_at], [updated_at]) VALUES (5, N'2019 - 2020', CAST(N'2019-03-24' AS Date), CAST(N'2019-03-24' AS Date))
SET IDENTITY_INSERT [dbo].[Table_NamHoc] OFF
INSERT [dbo].[Table_NhanVien] ([MaNhanVien], [HoNhanVien], [TenNhanVien], [NgaySinh], [GioiTinh], [DiaChi], [SoCMND], [SDT], [Email], [HinhAnh], [NamVaoLam], [created_at], [updated_at], [ChucVu_MCV], [PhongBan_MPB], [TrinhDo_MTD], [ChuyenNganh_MCN]) VALUES (N'NV001', N'Nguyễn', N'Tấn Thành', CAST(N'2018-11-08' AS Date), 1, N'Bình Dương', 123456789, 937519106, N'tanthanhphuochoa@gmail.com', N'iconprofile09042019.png', 2015, CAST(N'2018-11-13' AS Date), CAST(N'2018-11-13' AS Date), N'CV08', N'PB01', N'TDHV01', N'CN01')
INSERT [dbo].[Table_NhanVien] ([MaNhanVien], [HoNhanVien], [TenNhanVien], [NgaySinh], [GioiTinh], [DiaChi], [SoCMND], [SDT], [Email], [HinhAnh], [NamVaoLam], [created_at], [updated_at], [ChucVu_MCV], [PhongBan_MPB], [TrinhDo_MTD], [ChuyenNganh_MCN]) VALUES (N'NV002', N'Demo', N'Nguoi', CAST(N'2018-11-08' AS Date), 1, N'123', 123, 123123, N'tanthanhphuochoa@gmail.com', N'avatardefault06042019.png', 2016, CAST(N'2018-11-24' AS Date), NULL, N'CV07', N'PB01', N'TDHV01', N'CN01')
INSERT [dbo].[Table_NhanVien] ([MaNhanVien], [HoNhanVien], [TenNhanVien], [NgaySinh], [GioiTinh], [DiaChi], [SoCMND], [SDT], [Email], [HinhAnh], [NamVaoLam], [created_at], [updated_at], [ChucVu_MCV], [PhongBan_MPB], [TrinhDo_MTD], [ChuyenNganh_MCN]) VALUES (N'NV003', N'Nguyễn', N'Thành', CAST(N'2019-02-06' AS Date), 1, N'Phước Hòa - Phú Giáo - Bình Dương', 1, 937519106, N'tanthanhphuochoa@gmail.com', N'avtstudent02032019.png', 2017, CAST(N'2018-11-13' AS Date), CAST(N'2019-02-17' AS Date), N'CV10', N'PB02', N'TDHT02', N'CN01')
INSERT [dbo].[Table_NhanVien] ([MaNhanVien], [HoNhanVien], [TenNhanVien], [NgaySinh], [GioiTinh], [DiaChi], [SoCMND], [SDT], [Email], [HinhAnh], [NamVaoLam], [created_at], [updated_at], [ChucVu_MCV], [PhongBan_MPB], [TrinhDo_MTD], [ChuyenNganh_MCN]) VALUES (N'NV004', N'ACE', N'AVE', CAST(N'2018-12-05' AS Date), 1, N'123', 123, 123, N'tanthanhphuochoa@gmail.com', N'iconprofile09042019.png', 2018, CAST(N'2018-12-06' AS Date), NULL, N'CV06', N'PB02', N'TDHV01', N'CN01')
INSERT [dbo].[Table_NhanVien] ([MaNhanVien], [HoNhanVien], [TenNhanVien], [NgaySinh], [GioiTinh], [DiaChi], [SoCMND], [SDT], [Email], [HinhAnh], [NamVaoLam], [created_at], [updated_at], [ChucVu_MCV], [PhongBan_MPB], [TrinhDo_MTD], [ChuyenNganh_MCN]) VALUES (N'NV005', N'Trịnh', N'Trọng Thành', CAST(N'2001-03-22' AS Date), 1, N'123', 123, 123123, N'tanthanhphuochoa123@gmail.com', N'avatardefault06042019.png', 2015, CAST(N'2019-01-04' AS Date), NULL, N'CV06', N'PB01', N'TDHV01', N'CN01')
INSERT [dbo].[Table_PhongBan] ([MaPhongBan], [TenPhongBan], [HinhAnh], [created_at], [updated_at]) VALUES (N'PB01', N'Kỹ Thuật - Công Nghệ', NULL, CAST(N'2018-11-10' AS Date), NULL)
INSERT [dbo].[Table_PhongBan] ([MaPhongBan], [TenPhongBan], [HinhAnh], [created_at], [updated_at]) VALUES (N'PB02', N'Công Tác Sinh Viên', NULL, CAST(N'2018-11-12' AS Date), NULL)
INSERT [dbo].[Table_PhongBan] ([MaPhongBan], [TenPhongBan], [HinhAnh], [created_at], [updated_at]) VALUES (N'PB03', N'Khoa Học Quản Lý', NULL, CAST(N'2019-02-26' AS Date), NULL)
INSERT [dbo].[Table_PhongBan] ([MaPhongBan], [TenPhongBan], [HinhAnh], [created_at], [updated_at]) VALUES (N'PB04', N'Khoa Sư Phạm', NULL, CAST(N'2019-02-26' AS Date), NULL)
INSERT [dbo].[Table_PhongBan] ([MaPhongBan], [TenPhongBan], [HinhAnh], [created_at], [updated_at]) VALUES (N'PB05', N'Khoa Kinh Tế', NULL, CAST(N'2019-02-26' AS Date), NULL)
INSERT [dbo].[Table_PhongBan] ([MaPhongBan], [TenPhongBan], [HinhAnh], [created_at], [updated_at]) VALUES (N'PB06', N'Khoa Ngoại Ngữ', NULL, CAST(N'2019-02-26' AS Date), NULL)
SET IDENTITY_INSERT [dbo].[Table_QuyetDinhTDKT] ON 

INSERT [dbo].[Table_QuyetDinhTDKT] ([MaQuyetDinh], [TenQuyetDinh], [Link], [NgayBanHanh], [created_at], [updated_at]) VALUES (1, N'Quyết định 1708/QĐ-TTg về việc tặng danh hiệu Chiến sĩ Thi đua toàn quốc cho ông Nguyễn Ngọc Tuyến, Viện trưởng Viện Kiểm sát nhân dân tỉnh Hà Nam', N'https://drive.google.com/open?id=1t20pZ0bS2foYMHOaSnBPV0hgabbtzNL6', CAST(N'2018-06-19' AS Date), CAST(N'2019-02-06' AS Date), NULL)
INSERT [dbo].[Table_QuyetDinhTDKT] ([MaQuyetDinh], [TenQuyetDinh], [Link], [NgayBanHanh], [created_at], [updated_at]) VALUES (2, N'Quyết định 1703/QĐ-TTg về việc tặng Cờ Thi đua của Chính phủ cho 7 tập thể thuộc thành phố Hà Nội', N'https://drive.google.com/open?id=1t20pZ0bS2foYMHOaSnBPV0hgabbtzNL6', CAST(N'2019-01-08' AS Date), CAST(N'2019-02-06' AS Date), NULL)
INSERT [dbo].[Table_QuyetDinhTDKT] ([MaQuyetDinh], [TenQuyetDinh], [Link], [NgayBanHanh], [created_at], [updated_at]) VALUES (3, N'Quyết định 1690/QĐ-TTg về việc tặng Bằng khen Thủ tướng Chính phủ cho 2 cá nhân thuộc Bộ Công an', N'https://drive.google.com/open?id=1kDaXXVV5IEfrrXpgHiP04Lo9ZBT0Y0sM', CAST(N'2016-06-10' AS Date), CAST(N'2019-02-06' AS Date), NULL)
SET IDENTITY_INSERT [dbo].[Table_QuyetDinhTDKT] OFF
SET IDENTITY_INSERT [dbo].[Table_SangKienKinhNghiem] ON 

INSERT [dbo].[Table_SangKienKinhNghiem] ([MaSKTN], [TenSKTN], [AnhMinhChung], [SoDiem], [KiemDuyet], [created_at], [updated_at], [NhanVien_MNV], [NamHoc_MNH]) VALUES (11, N'Sáng kiến Hệ Thống', N'detainckh10052019.PNG', 1.5, 1, CAST(N'2019-05-10' AS Date), NULL, N'NV003', 5)
SET IDENTITY_INSERT [dbo].[Table_SangKienKinhNghiem] OFF
SET IDENTITY_INSERT [dbo].[Table_SuKien] ON 

INSERT [dbo].[Table_SuKien] ([MaSuKien], [TenSuKien], [NoiDung], [HinhAnh], [SoLuotXem], [created_at], [updated_at]) VALUES (1, N'Trao đổi học thuật giữa Viện Phát triển KHCN và Khoa Kỹ thuật hạt nhân', N'<p>
	Trao đổi học thuật giữa Viện Ph&aacute;t triển KHCN v&agrave; Khoa Kỹ thuật hạt nh&acirc;n</p>
', N'sukienhatnhan05032019.jpg', 7, CAST(N'2019-03-05' AS Date), NULL)
INSERT [dbo].[Table_SuKien] ([MaSuKien], [TenSuKien], [NoiDung], [HinhAnh], [SoLuotXem], [created_at], [updated_at]) VALUES (2, N'Báo cáo chuyên đề “Các thủ tục được chính thức hóa: Sở hữu trí tuệ, Khung hợp đồng”', N'<p>
	B&aacute;o c&aacute;o chuy&ecirc;n đề &ldquo;C&aacute;c thủ tục được ch&iacute;nh thức h&oacute;a: Sở hữu tr&iacute; tuệ, Khung hợp đồng&rdquo; ACE</p>
', N'hoatdongtritue05032019.JPG', 5, CAST(N'2019-03-05' AS Date), CAST(N'2019-03-05' AS Date))
INSERT [dbo].[Table_SuKien] ([MaSuKien], [TenSuKien], [NoiDung], [HinhAnh], [SoLuotXem], [created_at], [updated_at]) VALUES (3, N'Làm việc với Viện Khoa học vật liệu ứng dụng về phát triển hợp tác đào tạo và nghiên cứu', N'<p>
	L&agrave;m việc với Viện Khoa học vật liệu ứng dụng về ph&aacute;t triển hợp t&aacute;c đ&agrave;o tạo v&agrave; nghi&ecirc;n cứu ACE</p>
', N'hoptacdaotaonghiencuu05032019.jpg', 1, CAST(N'2019-03-05' AS Date), CAST(N'2019-03-05' AS Date))
INSERT [dbo].[Table_SuKien] ([MaSuKien], [TenSuKien], [NoiDung], [HinhAnh], [SoLuotXem], [created_at], [updated_at]) VALUES (4, N'ĐH Thủ Dầu Một đẩy mạnh hoạt động nghiên cứu khoa học trong sinh viên: KIÊN TRÌ TRỒNG CÂY ẮT CÓ QUẢ NGỌT', N'<p>
	ĐH Thủ Dầu Một đẩy mạnh hoạt động nghi&ecirc;n cứu khoa học trong sinh vi&ecirc;n: KI&Ecirc;N TR&Igrave; TRỒNG C&Acirc;Y ẮT C&Oacute; QUẢ NGỌT</p>
', N'trongcay06042019.jpg', 15, CAST(N'2019-03-05' AS Date), CAST(N'2019-04-06' AS Date))
SET IDENTITY_INSERT [dbo].[Table_SuKien] OFF
SET IDENTITY_INSERT [dbo].[Table_ThoiDiem] ON 

INSERT [dbo].[Table_ThoiDiem] ([MaThoiDiem], [ThoiDiemKhenThuong], [created_at], [updated_at]) VALUES (1, 2013, NULL, NULL)
INSERT [dbo].[Table_ThoiDiem] ([MaThoiDiem], [ThoiDiemKhenThuong], [created_at], [updated_at]) VALUES (2, 2014, NULL, NULL)
INSERT [dbo].[Table_ThoiDiem] ([MaThoiDiem], [ThoiDiemKhenThuong], [created_at], [updated_at]) VALUES (3, 2015, NULL, NULL)
INSERT [dbo].[Table_ThoiDiem] ([MaThoiDiem], [ThoiDiemKhenThuong], [created_at], [updated_at]) VALUES (4, 2016, NULL, NULL)
INSERT [dbo].[Table_ThoiDiem] ([MaThoiDiem], [ThoiDiemKhenThuong], [created_at], [updated_at]) VALUES (5, 2017, NULL, NULL)
INSERT [dbo].[Table_ThoiDiem] ([MaThoiDiem], [ThoiDiemKhenThuong], [created_at], [updated_at]) VALUES (6, 2018, NULL, NULL)
INSERT [dbo].[Table_ThoiDiem] ([MaThoiDiem], [ThoiDiemKhenThuong], [created_at], [updated_at]) VALUES (12, 2019, NULL, NULL)
INSERT [dbo].[Table_ThoiDiem] ([MaThoiDiem], [ThoiDiemKhenThuong], [created_at], [updated_at]) VALUES (13, 2020, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Table_ThoiDiem] OFF
SET IDENTITY_INSERT [dbo].[Table_ThongBao] ON 

INSERT [dbo].[Table_ThongBao] ([MaThongBao], [NoiDung], [created_at], [updated_at]) VALUES (1, N'Hệ thống thi đua khen thưởng sẽ bảo trì vào 30/04/2019. LƯU Ý: Các cá nhân đăng tin trước thời điểm hệ thống bảo trì.', CAST(N'2019-03-19' AS Date), CAST(N'2019-04-09' AS Date))
SET IDENTITY_INSERT [dbo].[Table_ThongBao] OFF
INSERT [dbo].[Table_TinTuc] ([MaTinTuc], [TenTinTuc], [NoiDung], [HinhAnh], [NoiBat], [SoLuotXem], [created_at], [updated_at]) VALUES (N'TT001', N'Ký kết MOU trong lĩnh vực công nghệ sinh học giữa TDMU và công ty R&D', N'<p>
	<span style="box-sizing: border-box; font-weight: 700; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">S&aacute;ng ng&agrave;y 5/12/2018, trường Đại học Thủ Dầu Một v&agrave; c&ocirc;ng ty Cổ phần N&ocirc;ng nghiệp R&amp;D đ&atilde; ch&iacute;nh thức k&yacute; kết thỏa thuận hợp t&aacute;c trong việc nghi&ecirc;n cứu v&agrave; triển khai c&aacute;c đề t&agrave;i, dự &aacute;n trong lĩnh vực khoa học tự nhi&ecirc;n, sinh học ứng dụng, hỗ trợ sinh vi&ecirc;n khởi nghiệp.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Tham dự buổi lễ, về ph&iacute;a C&ocirc;ng ty Cổ phần N&ocirc;ng nghiệp R&amp;D c&oacute; &ocirc;ng Ch&acirc;u Minh Chinh &ndash; Gi&aacute;m đốc, c&ugrave;ng c&aacute;c cộng sự. Về ph&iacute;a trường Đại học Thủ Dầu Một c&oacute; PGS.TS Nguyễn Văn Hiệp &ndash; Chủ tịch Hội đồng Trường, PGS.TS Ho&agrave;ng Trọng Quyền &ndash; Ph&oacute; Hiệu trưởng, c&ugrave;ng l&atilde;nh đạo ph&ograve;ng Khoa học, H&agrave;nh ch&iacute;nh, khoa Khoa học Tự nhi&ecirc;n, Viện Ph&aacute;t triển khoa học v&agrave; c&ocirc;ng nghệ, Trung t&acirc;m Nghi&ecirc;n cứu Thực nghiệm.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Với tinh thần hợp t&aacute;c tr&ecirc;n cơ sở mang lại lợi &iacute;ch gi&aacute;o dục, thương mại cho hai b&ecirc;n v&agrave; cho cộng đồng, sau một thời gian trao đổi, đ&agrave;m ph&aacute;n, trường Đại học Thủ Dầu Một v&agrave; C&ocirc;ng ty Cổ phần N&ocirc;ng nghiệp R&amp;D đ&atilde; thống nhất c&aacute;c điều kiện thỏa thuận để trở th&agrave;nh đối t&aacute;c chiến lược của nhau trong việc nghi&ecirc;n cứu v&agrave; triển khai c&aacute;c đề t&agrave;i, dự &aacute;n trong lĩnh vực khoa học tự nhi&ecirc;n, sinh học ứng dụng, hỗ trợ sinh vi&ecirc;n khởi nghiệp.&nbsp;</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">C&aacute;c mục ti&ecirc;u của thỏa thuận hợp t&aacute;c gồm c&oacute;, hai b&ecirc;n sẽ c&ugrave;ng nhau phối hợp tổ chức c&aacute;c kh&oacute;a huấn luyện, bồi dưỡng, chia sẻ th&ocirc;ng tin, kiến thức mới về khoa học, c&ocirc;ng nghệ cho c&aacute;n bộ, giảng vi&ecirc;n, nh&acirc;n vi&ecirc;n; Đại học Thủ Dầu Một mời c&aacute;n bộ, chuy&ecirc;n gia của C&ocirc;ng ty Cổ phần N&ocirc;ng nghiệp R&amp;D tham gia giảng dạy, hướng dẫn nghi&ecirc;n cứu khoa học, hướng dẫn thực h&agrave;nh, thực tập cho sinh vi&ecirc;n; C&ocirc;ng ty Cổ phần N&ocirc;ng nghiệp R&amp;D tham gia đ&oacute;ng g&oacute;p &yacute; kiến g&oacute;p phần ho&agrave;n chỉnh v&agrave; ph&aacute;t triển chương tr&igrave;nh đ&agrave;o tạo của trường Đại học Thủ Dầu Một theo ti&ecirc;u ch&iacute; b&aacute;m s&aacute;t y&ecirc;u cầu của thị trường lao động trong lĩnh vực c&ocirc;ng nghệ sinh học, tiếp nhận, tạo điều kiện cho sinh vi&ecirc;n đến thực h&agrave;nh, thực tập tại C&ocirc;ng ty.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Ph&aacute;t biểu ch&uacute;c mừng sự hợp t&aacute;c chiến lược của hai b&ecirc;n, PGS. TS Nguyễn Văn Hiệp &nbsp;tin tưởng rằng, với kinh nghiệm cũng như danh tiếng của m&igrave;nh, Đại học Thủ Dầu Một sẽ c&ugrave;ng với R&amp;D khai th&aacute;c hiệu quả c&aacute;c thế mạnh m&agrave; hai đơn vị đang sở hữu, đưa quan hệ hợp t&aacute;c giữa hai b&ecirc;n đạt nhiều thắng lợi. &nbsp;</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Trong khu&ocirc;n khổ của lễ k&yacute; kết, l&atilde;nh đạo hai b&ecirc;n cũng đ&atilde; d&agrave;nh thời gian trao đổi c&aacute;c th&ocirc;ng tin về dự &aacute;n triển khai nghi&ecirc;n cứu giống b&ograve; cho tỉnh Champasak, nghi&ecirc;n cứu v&agrave; ph&aacute;t triển c&aacute;c giống c&acirc;y trồng n&ocirc;ng nghiệp như phong lan, tảo&hellip;; kế hoạch hỗ trợ sinh vi&ecirc;n khởi nghiệp trong lĩnh vực sinh học ứng dụng, sinh học n&ocirc;ng nghiệp.</span></p>
', N'kyketMOU09042019.JPG', 1, NULL, CAST(N'2019-04-09' AS Date), NULL)
INSERT [dbo].[Table_TinTuc] ([MaTinTuc], [TenTinTuc], [NoiDung], [HinhAnh], [NoiBat], [SoLuotXem], [created_at], [updated_at]) VALUES (N'TT002', N'Thiết lập quan hệ hợp tác với Quỹ hỗ trợ Nghiên cứu khoa học – Công nghệ', N'<p>
	<span style="box-sizing: border-box; font-weight: 700; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Ng&agrave;y 24/3/2019, PGS.TS Nguyễn Văn Hiệp &ndash; CTHĐ Trường đ&atilde; chủ tr&igrave; buổi l&agrave;m việc với &ocirc;ng Nguyễn Đức Dũng &ndash; Gi&aacute;m đốc Dự &aacute;n hợp t&aacute;c đại học khu vực miền Trung, miền Nam</span><span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">&nbsp;</span><span style="box-sizing: border-box; font-weight: 700; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">thuộc Tập đo&agrave;n Vingroup nhằm b&agrave;n thảo c&aacute;c cơ hội hợp t&aacute;c trong hoạt động nghi&ecirc;n cứu khoa học v&agrave; chuyển giao c&ocirc;ng nghệ. Tham dự buổi họp c&oacute; l&atilde;nh đạo ph&ograve;ng Khoa học, khoa KHTN, viện Khoa học C&ocirc;ng nghệ, trung t&acirc;m Nghi&ecirc;n cứu thực nghiệm.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Trao đổi tại buổi l&agrave;m việc, &ocirc;ng Nguyễn Đức Dũng cho biết, với mục ti&ecirc;u trở th&agrave;nh một Tập đo&agrave;n C&ocirc;ng nghệ - C&ocirc;ng nghiệp &ndash; Dịch vụ đẳng cấp quốc tế, Vingroup đ&atilde; th&agrave;nh lập Quỹ hỗ trợ Nghi&ecirc;n cứu khoa học &ndash; C&ocirc;ng nghệ ứng dụng nhằm đồng h&agrave;nh c&ugrave;ng c&aacute;c dự &aacute;n nghi&ecirc;n cứu của giảng vi&ecirc;n, sinh vi&ecirc;n trong nước; t&agrave;i trợ học bổng cho c&aacute;c sinh vi&ecirc;n t&agrave;i năng trong lĩnh vực kỹ thuật c&ocirc;ng nghệ. Ch&iacute;nh v&igrave; vậy, Quỹ được kỳ vọng sẽ th&uacute;c đẩy hoạt động nghi&ecirc;n cứu v&agrave; thực nghiệm c&aacute;c nghi&ecirc;n cứu khoa học c&ocirc;ng nghệ, g&oacute;p phần n&acirc;ng cao năng lực của đội ngũ giảng vi&ecirc;n l&agrave;m nghi&ecirc;n cứu khoa học, của c&aacute;c kỹ sư tương lai. &Ocirc;ng cũng đ&atilde; th&ocirc;ng tin những g&oacute;i t&agrave;i trợ của Quỹ cho c&aacute;c dự &aacute;n thuộc lĩnh vực: khoa học m&aacute;y t&iacute;nh, tr&iacute; tuệ nh&acirc;n tạo, robotics, tự động h&oacute;a, c&ocirc;ng nghệ nano, năng lượng t&aacute;i tạo, nguy&ecirc;n liệu thế hệ mới, sinh th&aacute;i bền vững. PGS hy vọng, th&ocirc;ng qua trao đổi, t&igrave;m kiếm c&aacute;c cơ hội, hai đơn vị sẽ đạt được những thỏa thuận hợp t&aacute;c thuộc c&aacute;c lĩnh vực nghi&ecirc;n cứu m&agrave; hai b&ecirc;n c&oacute; nhiều thế mạnh v&agrave; sự tương đồng.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Đ&aacute;p lời, PGS.TS Nguyễn Văn Hiệp cảm ơn sự quan t&acirc;m của Quỹ hỗ trợ Nghi&ecirc;n cứu khoa học &ndash; C&ocirc;ng nghệ đến c&aacute;c th&agrave;nh tựu v&agrave; định hướng của hoạt động nghi&ecirc;n cứu khoa học v&agrave; chuyển giao c&ocirc;ng nghệ của Trường. Qua th&ocirc;ng tin trao đổi về c&aacute;c chương tr&igrave;nh đầu tư, hợp t&aacute;c nghi&ecirc;n cứu của Quỹ, PGS.TS Nguyễn Văn Hiệp vui mừng cho biết, thế mạnh nghi&ecirc;n cứu thuộc lĩnh vực sinh th&aacute;i bền vững của Trường c&oacute; nhiều điểm tương th&iacute;ch v&agrave; ph&ugrave; hợp với c&aacute;c điều kiện hợp t&aacute;c của Quỹ. PGS cho biết th&ecirc;m, Trường đ&atilde; h&igrave;nh th&agrave;nh v&agrave; ph&aacute;t triển nh&oacute;m nghi&ecirc;n cứu về n&ocirc;ng nghiệp cao &ndash; đ&ocirc; thị; B&igrave;nh Dương &ndash; Th&agrave;nh phố th&ocirc;ng minh; Đ&ocirc;ng Nam bộ - V&ugrave;ng kinh tế trọng điểm ph&iacute;a Nam; Khai th&aacute;c v&agrave; xử l&yacute; dữ liệu &ndash; HPC (JVU &ndash; TDMU); Đại học th&ocirc;ng minh&hellip; Trong đ&oacute;, hoạt động nghi&ecirc;n cứu của lĩnh vực sinh th&aacute;i bền vững đ&atilde; đạt được nhiều th&agrave;nh tựu t&iacute;ch cực v&agrave; được chuyển giao cho nhiều doanh nghiệp, địa phương trong nước v&agrave; nước ngo&agrave;i, như: Đ&ocirc;ng tr&ugrave;ng hạ thảo; nấm dược liệu, l&agrave;m giấy từ nước dừa, nước thanh long; c&aacute;c chế phẩm n&ocirc;ng nghiệp&hellip;</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">B&agrave;n thảo th&ecirc;m về c&aacute;c định hướng hợp t&aacute;c, PGS.TS Nguyễn Văn Hiệp cũng đ&atilde; th&ocirc;ng tin về chiến lược ph&aacute;t triển quy m&ocirc; đ&agrave;o tạo với đề xuất mở m&atilde; ng&agrave;nh Kỹ sư &ocirc; t&ocirc;, chương tr&igrave;nh nghi&ecirc;n cứu si&ecirc;u m&aacute;y t&iacute;nh phục vụ cho hoạt động nghi&ecirc;n cứu, giảng dạy, học tập tại Trường&hellip; PGS kỳ vọng, với lợi thế về tiềm lực khoa học c&ocirc;ng nghệ, thương hiệu khoa học gi&aacute;o dục, năng lực triển khai hiệu quả v&agrave; đội ngũ c&aacute;n bộ l&agrave;m khoa học nhiệt huyết, Đại học Thủ Dầu Một mong muốn sẽ trở th&agrave;nh đối t&aacute;c chiến lược với Quỹ hỗ trợ Nghi&ecirc;n cứu khoa học &ndash; C&ocirc;ng nghệ nhằm tạo điều kiện thuận lợi để c&oacute; thể nhanh ch&oacute;ng triển khai những &yacute; tưởng, định hướng nghi&ecirc;n cứu của Trường đi v&agrave;o thực tiễn, g&oacute;p phần mang lại những hiệu quả thiết thực cho Nh&agrave; trường &ndash; Doanh nghiệp &ndash; Cộng đồng.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Theo thỏa ước, hai b&ecirc;n sẽ c&ugrave;ng th&uacute;c đẩy c&aacute;c chương tr&igrave;nh hợp t&aacute;c thuộc c&aacute;c g&oacute;i dự &aacute;n được Quỹ t&agrave;i trợ; đề &aacute;n hợp t&aacute;c ph&aacute;t triển c&aacute;c chương tr&igrave;nh nghi&ecirc;n cứu về sinh th&aacute;i bền vững, khoa học m&aacute;y t&iacute;nh; kế hoạch phối hợp tổ chức hội thảo khoa học, tọa đ&agrave;m chuy&ecirc;n gia; hỗ trợ sinh vi&ecirc;n thực h&agrave;nh, thực tập&hellip;</span></p>
', N'quanhequyhotro09042019.JPG', 1, NULL, CAST(N'2019-04-09' AS Date), NULL)
INSERT [dbo].[Table_TinTuc] ([MaTinTuc], [TenTinTuc], [NoiDung], [HinhAnh], [NoiBat], [SoLuotXem], [created_at], [updated_at]) VALUES (N'TT003', N'Trao đổi học thuật giữa Viện Phát triển KHCN và Khoa Kỹ thuật hạt nhân', N'<p>
	<span style="box-sizing: border-box; font-weight: 700; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Hiện thực h&oacute;a c&aacute;c nội dung k&yacute; kết MOU của hai đơn vị, ng&agrave;y 27/02/2019, c&aacute;c chuy&ecirc;n gia trường Đại học Thủ Dầu Một đ&atilde; c&oacute; buổi trao đổi chuy&ecirc;n đề với c&aacute;c chuy&ecirc;n gia khoa Kỹ thuật hạt nh&acirc;n của Đại học Đ&agrave; Lạt.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Tại buổi trao đồi học thuật, PGS.TS Nguyễn Thanh B&igrave;nh &ndash; Viện trưởng Viện ph&aacute;t triển Khoa học c&ocirc;ng nghệ c&ugrave;ng c&aacute;c chuy&ecirc;n gia đến từ Trung t&acirc;m nghi&ecirc;n cứu thực nghiệm, khoa Khoa học tự nhi&ecirc;n đ&atilde; chia sẻ th&ocirc;ng tin khoa học xoay quanh chủ đề &ldquo;Giới thiệu c&aacute;c sản phẩm ứng dụng KHCN v&agrave; chuyển giao cho doanh nghiệp&rdquo;. Về ph&iacute;a khoa Kỹ thuật hạt nh&acirc;n, với thế mạnh của m&igrave;nh, l&atilde;nh đạo v&agrave; c&aacute;c giảng vi&ecirc;n của Khoa đ&atilde; trao đổi chuy&ecirc;n s&acirc;u về chủ đề &ldquo;Sử dụng kỹ thuật hạt nh&acirc;n trong b&aacute;n hủy Polyme&rdquo;. B&ecirc;n cạnh c&aacute;c nội dung học thuật, hai đơn vị cũng trao đổi v&agrave; thống nhất h&igrave;nh th&agrave;nh c&aacute;c chương tr&igrave;nh nghi&ecirc;n cứu chung, tạo m&ocirc;i trường khoa học cho sinh vi&ecirc;n được học tập, l&agrave;m việc với c&aacute;c nh&agrave; nghi&ecirc;n cứu trong v&agrave; ngo&agrave;i nước.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">C&ugrave;ng ng&agrave;y, đo&agrave;n chuy&ecirc;n gia của Trường tiếp tục l&agrave;m việc với khoa Sinh học (Đại học Đ&agrave; Lạt) để t&igrave;m hiểu kỹ thuật nu&ocirc;i cấy m&ocirc;, tham quan thực c&aacute;c m&ocirc; h&igrave;nh n&ocirc;ng nghiệp chuy&ecirc;n canh, đồng thời tư vấn cho c&aacute;c hộ n&ocirc;ng d&acirc;n. Trong thời gian tới, c&aacute;c sản phẩm n&ocirc;ng nghiệp của Trường Đại học Thủ Dầu Một sẽ được triển khai trồng tại khu chuy&ecirc;n canh n&ocirc;ng nghiệp c&ocirc;ng nghệ cao tại Đ&agrave; Lạt.</span></p>
', N'vienphattrienkhcn09042019.jpg', 1, NULL, CAST(N'2019-04-09' AS Date), NULL)
INSERT [dbo].[Table_TinTuc] ([MaTinTuc], [TenTinTuc], [NoiDung], [HinhAnh], [NoiBat], [SoLuotXem], [created_at], [updated_at]) VALUES (N'TT004', N'Làm việc với Viện Khoa học vật liệu ứng dụng về phát triển hợp tác đào tạo và nghiên cứu', N'<p>
	<span style="box-sizing: border-box; font-weight: 700; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">S&aacute;ng 29/3/2018,&nbsp;</span><span style="box-sizing: border-box; font-weight: 700; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Đo&agrave;n c&ocirc;ng t&aacute;c khoa Khoa học Tự nhi&ecirc;n do PGS.TS Nguyễn Thanh B&igrave;nh &ndash; Trưởng khoa l&agrave;mTrưởng đo&agrave;n đ&atilde; c&oacute; chuyến thăm v&agrave; l&agrave;m việc tại Viện Khoa học vật liệu ứng dụng - Viện H&agrave;n l&acirc;m Khoa học C&ocirc;ng nghệ Việt Nam</span><span style="box-sizing: border-box; font-weight: 700; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">. Tiếp Đo&agrave;n c&oacute; PGS.TS. Nguyễn Mạnh Tuấn &ndash; Viện trưởng, c&ugrave;ng l&atilde;nh đạo c&aacute;c ph&ograve;ng ban thuộc Viện.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">PGS.TS Nguyễn Thanh B&igrave;nh giới thiệu đến L&atilde;nh đạo Viện về chiến lược ph&aacute;t triển đến năm 2020 của Trường v&agrave; Khoa. Theo đ&oacute;, Đại học Thủ Dầu Một ph&aacute;t triển theo định hướng ứng dụng, tiếp cận c&aacute;c ti&ecirc;u chuẩn gi&aacute;o dục đại học trong khu vực v&agrave; quốc tế. Trường đặc biệt ch&uacute; trọng c&aacute;c sản phẩm nghi&ecirc;n cứu khoa học c&oacute; t&iacute;nh ứng dụng cao, phục vụ thiết thực cho cuộc sống con người, qua đ&oacute; từng bước khẳng định vị tr&iacute; l&agrave; trung t&acirc;m nghi&ecirc;n cứu khoa học c&oacute; chất lượng của Tỉnh v&agrave; khu vực Đ&ocirc;ng Nam Bộ.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Trong khu&ocirc;n khổ buổi l&agrave;m việc, sau khi nghe giới thiệu về c&aacute;c lĩnh vực vật liệu mới, c&ocirc;ng nghệ nano, vật liệu y sinh, vật liệu m&ocirc;i trường của Trường Đại học Thủ Dầu Một, PGS.TS. Nguyễn Mạnh Tuấn &ndash; Ph&oacute; Viện trưởng đ&aacute;nh gi&aacute; cao những kết quả đạt được v&agrave; hướng đi của Trường trong thời gian tới. Đồng thời, PGS ủng hộ sự hợp t&aacute;c giữa Viện v&agrave; Trường. PGS khuyến kh&iacute;ch h&igrave;nh th&agrave;nh những nh&oacute;m nghi&ecirc;n cứu khoa học li&ecirc;n ng&agrave;nh, trong đ&oacute;, học vi&ecirc;n cao học v&agrave; sinh vi&ecirc;n c&aacute;c khối ng&agrave;nh khoa học tự nhi&ecirc;n của Đại học Thủ Dầu Một c&oacute; thể tham gia, gi&uacute;p n&acirc;ng cao năng lực nghi&ecirc;n cứu của bản th&acirc;n, c&ugrave;ng Trường thực hiện hiệu quả đề &aacute;n nghi&ecirc;n cứu Th&agrave;nh phố th&ocirc;ng minh B&igrave;nh Dương v&agrave; đ&aacute;p ứng c&ocirc;ng nghệ 4.0.</span></p>
', N'khoahocvatlieu09042019.jpg', 1, NULL, CAST(N'2019-04-09' AS Date), NULL)
INSERT [dbo].[Table_TinTuc] ([MaTinTuc], [TenTinTuc], [NoiDung], [HinhAnh], [NoiBat], [SoLuotXem], [created_at], [updated_at]) VALUES (N'TT005', N'Hội thảo “Kiến trúc và xây dựng hướng đến phát triển bền vững”', N'<div style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">
	<span style="box-sizing: border-box; font-weight: 700;">Ng&agrave;y 17/8/2018, trường Đại học Thủ Dầu Một đ&atilde; tổ chức th&agrave;nh c&ocirc;ng hội thảo khoa học với chủ đề &ldquo;Kiến tr&uacute;c v&agrave; x&acirc;y dựng hướng đến ph&aacute;t triển bền vững&rdquo;, với sự tham dự của gần 200 kiến tr&uacute;c sư, kỹ sư, c&aacute;c nh&agrave; nghi&ecirc;n cứu trong lĩnh vực kiến tr&uacute;c, x&acirc;y dựng v&agrave; quy hoạch.</span><br style="box-sizing: border-box;" />
	<br style="box-sizing: border-box;" />
	Hội thảo tập hợp hơn 40 b&agrave;i tham luận của nhiều học giả l&agrave; nh&agrave; nghi&ecirc;n cứu, chuy&ecirc;n gia, nh&agrave; quản l&yacute;, giảng vi&ecirc;n, học vi&ecirc;n đến từ c&aacute;c trường đại học, học viện, cao đẳng, v&agrave; c&aacute;c c&ocirc;ng ty hoạt động trong lĩnh vực kiến tr&uacute;c v&agrave; x&acirc;y dựng như: Đại học Kiến tr&uacute;c TP.HCM, Đại học Sư phạm Kỹ thuật TP.HCM, Đại học Mở TP.HCM, Đại học Khoa học (Đại học Huế), Đại học B&aacute;ch khoa Đ&agrave; Nẵng, Đại học Ng&ocirc; Quyền, Đại học X&acirc;y dựng Miền T&acirc;y, Đại học B&agrave; Rịa &ndash; Vũng T&agrave;u, Đại học Kỹ thuật &ndash; C&ocirc;ng nghệ Cần Thơ, Đại học Quốc gia Yokohama (Nhật Bản), C&ocirc;ng ty Chip EngSeng Contractors (Singapore)&hellip;<br style="box-sizing: border-box;" />
	<br style="box-sizing: border-box;" />
	Nội dung c&aacute;c b&agrave;i tham luận tập trung v&agrave;o c&aacute;c vấn đề về ph&aacute;t triển bền vững của quy hoạch iến tr&uacute;c v&agrave; x&acirc;y dựng trong bối cảnh t&aacute;c động của biến đổi kh&iacute; hậu ng&agrave;y c&agrave;ng diễn ra mạnh mẽ hiện nay. Tr&ecirc;n cơ sở nội dung c&aacute;c tham luận gửi đến, Ban Tổ chức đ&atilde; tuyển chọn 35 b&agrave;i viết để in trong kỷ yếu hội thảo c&oacute; m&atilde; số ti&ecirc;u chuẩn ISBN, được xuất bản bởi Nh&agrave; xuất bản Khoa học v&agrave; Kỹ thuật.<br style="box-sizing: border-box;" />
	<br style="box-sizing: border-box;" />
	Ph&aacute;t biểu khai mạc hội thảo, PGS.TS Ho&agrave;ng Trọng Quyền &ndash; Ph&oacute; Hiệu trưởng Nh&agrave; trường n&ecirc;u bật &yacute; nghĩa v&agrave; s&aacute;ng kiến tổ chức hội thảo của khoa Kiến tr&uacute;c- X&acirc;y dựng v&agrave; Mỹ thuật thuộc trường Đại học Thủ Dầu Một. PGS tin tưởng rằng, những gợi mở, kiến nghị, đề xuất tại hội thảo sẽ g&oacute;p phần v&agrave;o việc khơi gợi cảm hứng s&aacute;ng tạo v&agrave; nghi&ecirc;n cứu của tập thể giảng vi&ecirc;n, sinh vi&ecirc;n khoa Kiến tr&uacute;c &ndash; X&acirc;y dựng v&agrave; Mỹ thuật của Trường. Đồng thời, c&aacute;c kết quả nghi&ecirc;n cứu được c&ocirc;ng bố tại hội thảo kh&ocirc;ng chỉ đ&oacute;ng g&oacute;p t&iacute;ch cực cho qu&aacute; tr&igrave;nh ph&aacute;t triển bền vững ở B&igrave;nh Dương v&agrave; c&aacute;c địa phương l&acirc;n cận m&agrave; c&ograve;n l&agrave; nguồn học liệu v&ocirc; c&ugrave;ng qu&yacute; b&aacute;u, g&oacute;p phần phục vụ c&ocirc;ng t&aacute;c đ&agrave;o tạo c&aacute;c kỹ sư quy hoạch, c&aacute;c kiến tr&uacute;c sư c&ocirc;ng tr&igrave;nh v&agrave; kỹ sư x&acirc;y dựng tương lai.<br style="box-sizing: border-box;" />
	<br style="box-sizing: border-box;" />
	Sau phần b&aacute;o c&aacute;o đề dẫn, Ban Tổ chức đ&atilde; bố tr&iacute; 05 b&agrave;i tham luận tr&igrave;nh b&agrave;y trực tiếp tại phi&ecirc;n to&agrave;n thể hội thảo. Cụ thể, tham luận&nbsp;<em style="box-sizing: border-box;">&ldquo;Nghi&ecirc;n cứu kh&ocirc;ng gian xanh với biến động nhiệt độ bề mặt trong đ&ocirc; thị Huế&rdquo;</em>&nbsp;của TS. Trần Đ&igrave;nh Hiếu &ndash; trường Đại học Khoa học (Đại học Huế); tham luận&nbsp;<em style="box-sizing: border-box;">&ldquo;C&ocirc;ng nghệ chống thấm ngược cho tầng hầm&rdquo;</em>&nbsp;của &ocirc;ng Đỗ Th&agrave;nh T&iacute;ch &ndash; C&ocirc;ng ty Intoc; tham luận&nbsp;<em style="box-sizing: border-box;">&ldquo;Vệt hằn b&aacute;nh xe mặt đường b&ecirc; t&ocirc;ng nhựa &ndash; nguy&ecirc;n nh&acirc;n v&agrave; giải ph&aacute;p khắc phục</em>&rdquo; của TS. Nguyễn Huỳnh Tấn T&agrave;i &ndash; trường Đại học Thủ Dầu Một; tham luận &ldquo;<em style="box-sizing: border-box;">Giải ph&aacute;p hiệu quả năng lượng cho c&aacute;c c&ocirc;ng tr&igrave;nh sử dụng k&iacute;nh&rdquo;</em>&nbsp;của &ocirc;ng Huỳnh Nhật T&acirc;n &ndash; C&ocirc;ng ty Viglacera; tham luận&nbsp;<em style="box-sizing: border-box;">&ldquo;Sơn c&ocirc;ng nghệ xanh Terraco&rdquo;</em>&nbsp;của &ocirc;ng Nguyễn Thanh B&igrave;nh &ndash; C&ocirc;ng ty Terraco.<br style="box-sizing: border-box;" />
	<br style="box-sizing: border-box;" />
	Sau phi&ecirc;n to&agrave;n thể, hội thảo đ&atilde; chia th&agrave;nh 2 tiểu ban chuy&ecirc;n m&ocirc;n. Cụ thể, Tiểu ban 1 với chủ đề&nbsp;<span style="box-sizing: border-box; font-weight: 700;">&ldquo;Kỹ thuật v&agrave; vật liệu x&acirc;y dựng&rdquo;,</span>&nbsp;c&aacute;c t&aacute;c giả tham luận v&agrave; đại biểu đ&atilde; tập trung b&agrave;n thảo c&aacute;c vấn đề trọng t&acirc;m thuộc lĩnh vực x&acirc;y dựng như: nghi&ecirc;n cứu sử dụng phế thải cracking x&uacute;c t&aacute;c FCC của nh&agrave; m&aacute;y lọc dầu Dung Quất l&agrave;m phụ gia kho&aacute;ng để cải thiện một số t&iacute;nh chất cơ l&yacute; cho b&ecirc;t&ocirc;ng; vấn đề ảnh hưởng của qu&aacute; tr&igrave;nh Geopolymer đến cường độ chịu n&eacute;n của b&ecirc; t&ocirc;ng Geopolymer sử dụng tro bay; đ&aacute;nh gi&aacute; chất lượng c&aacute;t s&ocirc;ng tại khu vực đồng bằng S&ocirc;ng Cửu Long; m&ocirc; phỏng ứng xử của li&ecirc;n kết nối ống th&eacute;o tr&ograve;n sử dụng mặt b&iacute;ch v&agrave; bul&ocirc;ng chịu uốn cắt đồng thời; vấn đề tối ưu h&oacute;a trong thiết kế trắc dọc đường &ocirc; t&ocirc; bằng phương ph&aacute;p m&ocirc; phỏng số. Đối với Tiểu ban 2 c&oacute; chủ đề&nbsp;<span style="box-sizing: border-box; font-weight: 700;">&ldquo;Quy hoạch, kiến tr&uacute;c v&agrave; Mỹ thuật&rdquo;,</span>&nbsp;b&agrave;n về c&aacute;c yếu tố thẩm mỹ kiến tr&uacute;c cảnh quan tại c&aacute;c địa phương trong nước v&agrave; nước ngo&agrave;i; kiến tạo kh&ocirc;ng gian sinh hoạt cộng đồng bền vững trong qu&aacute; tr&igrave;nh đ&ocirc; thị h&oacute;a tại Việt Nam; vấn đề ứng dụng m&ocirc; h&igrave;nh th&ocirc;ng tin c&ocirc;ng tr&igrave;nh trong thiết kế kiến tr&uacute;c hiện nay.<br style="box-sizing: border-box;" />
	<br style="box-sizing: border-box;" />
	Sau 3 phi&ecirc;n l&agrave;m việc khẩn trương, nghi&ecirc;m t&uacute;c v&agrave; mang t&iacute;nh khoa học cao, c&aacute;c nh&oacute;m vấn đề trọng t&acirc;m thuộc lĩnh vực kiến tr&uacute;c v&agrave; x&acirc;y dựng hướng đến ph&aacute;t triển bền vững đ&atilde; được c&aacute;c nh&agrave; khoa học, nh&agrave; quản l&yacute; v&agrave; học giả trung nghi&ecirc;n cứu v&agrave; giải quyết như. Một số kết quả được tổng hợp trong b&aacute;o c&aacute;o tổng kết như: c&ocirc;ng t&aacute;c quy hoạch, thiết kế đ&ocirc; thị phải đảm bảo t&iacute;nh khoa học, coi trọng m&ocirc;i trường, cảnh quan thi&ecirc;n nhi&ecirc;n; c&ocirc;ng t&aacute;c thiết kế kiến tr&uacute;c c&ocirc;ng tr&igrave;nh theo hướng c&ocirc;ng tr&igrave;nh xanh, sử dụng c&aacute;c loại vật liệu c&oacute; t&iacute;nh năng cao, c&oacute; nguồn gốc th&acirc;n thiện với m&ocirc;i trường; &aacute;p dụng c&aacute;c giải ph&aacute;p t&iacute;nh to&aacute;n ch&iacute;nh x&aacute;c cho thiết kế kết cấu tối ưu, gi&uacute;p tiết kiệm thời gian, nguy&ecirc;n vật liệu v&agrave; bền vững cho c&ocirc;ng tr&igrave;nh; &aacute;p dụng c&ocirc;ng nghệ thi c&ocirc;ng ti&ecirc;n tiến gi&uacute;p tiết kiệm chi ph&iacute; nh&acirc;n c&ocirc;ng, hạn chế x&acirc;m lấn m&ocirc;i trường v&agrave; an to&agrave;n cho con người; sử dụng kỹ thuật duy tu, bảo dưỡng c&ocirc;ng tr&igrave;nh nhằm bảo tồn những c&ocirc;ng tr&igrave;nh hiện hữu tr&aacute;nh l&atilde;ng ph&iacute; cho việc x&acirc;y dựng mới, g&oacute;p phần giải quyết những vấn đề cấp thiết của lĩnh vực x&acirc;y dựng trong qu&aacute; tr&igrave;nh kinh tế, x&atilde; hội một c&aacute;ch h&agrave;i h&ograve;a v&agrave; bền vững.</div>
<div>
	&nbsp;</div>
<div style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: center;">
	&nbsp;</div>
', N'kientrucvaxaydung09042019.JPG', 1, NULL, CAST(N'2019-04-09' AS Date), NULL)
INSERT [dbo].[Table_TinTuc] ([MaTinTuc], [TenTinTuc], [NoiDung], [HinhAnh], [NoiBat], [SoLuotXem], [created_at], [updated_at]) VALUES (N'TT006', N'Thúc đẩy triển khai giáo dục STEM trong chương trình giáo dục', N'<p>
	<span style="box-sizing: border-box; font-weight: 700; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Ng&agrave;y 22/8/2018, PGS.TS Ho&agrave;ng Trọng Quyền &ndash; Ph&oacute; Hiệu trưởng đ&atilde; chủ tr&igrave; buổi tiếp v&agrave; l&agrave;m việc với&nbsp;</span><span style="box-sizing: border-box; font-weight: 700; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">tổ chức SaigonScientists (SgS) trao đổi về việc triển khai gi&aacute;o dục STEM.&nbsp;</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">C&ugrave;ng tiếp v&agrave; l&agrave;m việc với SgS c&oacute; TS. Trần Văn Trung &ndash; Trưởng ph&ograve;ng Khoa học, ThS. L&ecirc; Thị Kim &Uacute;t &ndash; Trưởng ph&ograve;ng Đ&agrave;o tạo Đại học, PGS.TS Nguyễn Thanh B&igrave;nh &ndash; Trưởng khoa Khoa học Tự nhi&ecirc;n, ThS. Vương Lợi &ndash; Ph&oacute; Gi&aacute;m đốc Trung t&acirc;m Nghi&ecirc;n cứu Thực nghiệm. Về ph&iacute;a SgS c&oacute; &ocirc;ng Phan Quốc Hải &ndash; Người s&aacute;ng lập, &ocirc;ng Alan West &ndash; Gi&aacute;m đốc điều h&agrave;nh.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Giới thiệu về tổ chức SgS, &ocirc;ng Phan Quốc Hải cho biết, SgS hoạt động với phương ch&acirc;m &ldquo;S&aacute;ng tạo để phục vụ&rdquo;. SgS mang lại những dịch vụ nghi&ecirc;n cứu ph&aacute;t triển, tư vấn chuyển giao c&ocirc;ng nghệ, hoạt động đ&agrave;o tạo cho c&aacute;c cơ sở đ&agrave;o tạo v&agrave; doanh nghiệp. Điểm mạnh của SgS l&agrave; tư vấn v&agrave; triển khai tập huấn phương ph&aacute;p gi&aacute;o dục theo định hướng STEM (Science; Technology; Engineering; Math) d&agrave;nh cho gi&aacute;o vi&ecirc;n THPT. &Ocirc;ng cho biết th&ecirc;m, STEM l&agrave; một chương tr&igrave;nh giảng dạy dựa tr&ecirc;n &yacute; tưởng trang bị cho người học những kiến thức, kỹ năng li&ecirc;n quan đến c&aacute;c lĩnh vực khoa học, c&ocirc;ng nghệ, kỹ thuật v&agrave; to&aacute;n học &ndash; theo c&aacute;ch tiếp cận li&ecirc;n m&ocirc;n (interdisciplinary) v&agrave; người học c&oacute; thể &aacute;p dụng để giải quyết vấn đề trong cuộc sống h&agrave;ng ng&agrave;y. Qua sự t&igrave;m hiểu về chiến lược ph&aacute;t triển của trường Đại học Thủ Dầu Một, SgS mong muốn được thiết lập mối quan hệ hợp t&aacute;c với Trường trong việc tư vấn, cung cấp c&aacute;c giải ph&aacute;p kết nối giữa trường đại học với c&aacute;c trường THPT th&ocirc;ng qua chương tr&igrave;nh gi&aacute;o dục STEM.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Thay mặt l&atilde;nh đạo Trường, PGS.TS Ho&agrave;ng Trọng Quyền đ&atilde; ph&aacute;t biểu ch&agrave;o mừng v&agrave; cảm ơn tổ chức SgS đ&atilde; c&oacute; đề xuất hợp t&aacute;c gi&uacute;p Trường ph&aacute;t triển chương tr&igrave;nh gi&aacute;o dục STEM. PGS chia sẻ th&ecirc;m, với mục ti&ecirc;u đưa &ldquo;ph&ograve;ng th&iacute; nghiệm vươn tới cộng đồng&rdquo;, g&oacute;p phần h&igrave;nh th&agrave;nh kỹ năng nghi&ecirc;n cứu khoa học ngay từ sớm cho c&aacute;c em học sinh THPT tr&ecirc;n địa b&agrave;n Tỉnh, Trường đ&atilde; chủ trương ph&aacute;t triển chương tr&igrave;nh gi&aacute;o dục STEM. Theo đ&oacute;, Trường đ&atilde; v&agrave; đang t&iacute;ch cực trển khai gi&aacute;o dục STEM ở c&aacute;c trường THPT th&ocirc;ng qua c&aacute;c hoạt động trải nghiệm thực nghiệm như: chương tr&igrave;nh &ldquo;Ng&agrave;y hội khoa học nh&iacute;&rdquo;, &ldquo;Ng&agrave;y hội Sinh vi&ecirc;n nghi&ecirc;n cứu khoa học&rdquo;, &ldquo;Em y&ecirc;u khoa học&rdquo;&hellip;. Đặc biệt, Trường đ&atilde; sớm th&agrave;nh lập c&aacute;c nh&oacute;m nghi&ecirc;n cứu về gi&aacute;o dục STEM với c&aacute;c nh&oacute;m ng&agrave;nh khoa học tự nhi&ecirc;n (H&oacute;a học, Sinh học ứng dụng, Vật l&yacute;, Khoa học m&ocirc;i trường); nh&oacute;m ng&agrave;nh kỹ thuật c&ocirc;ng nghệ (Điện &ndash; Điện tử, C&ocirc;ng nghệ th&ocirc;ng tin). Qua đề nghị hợp t&aacute;c của SgS, Trường nhận thấy c&oacute; nhiều điểm tương đồng với hoạt động ph&aacute;t triển chương gi&aacute;o dục STEM m&agrave; Trường đang thực hiện. PGS ho&agrave;n to&agrave;n ủng hộ v&agrave; tin tưởng hai b&ecirc;n sẽ c&oacute; những thỏa thuận hợp t&aacute;c gi&uacute;p Trường đưa ra được c&aacute;c giải ph&aacute;p triển khai gi&aacute;o dục STEM để đ&oacute;n đầu xu hướng ph&aacute;t triển gi&aacute;o dục, g&oacute;p phần gi&uacute;p Trường n&acirc;ng cao chất lượng sản phẩm đ&agrave;o tạo, đ&oacute;ng g&oacute;p nhiều hơn nữa cho sự ph&aacute;t triển gi&aacute;o dục ở nhiều cấp học kh&aacute;c nhau.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Trong khu&ocirc;n khổ buổi l&agrave;m việc, l&atilde;nh đạo hai b&ecirc;n đ&atilde; d&agrave;nh nhiều thời gian b&agrave;n thảo c&aacute;c giải ph&aacute;p ph&aacute;t triển chương tr&igrave;nh hợp t&aacute;c. Cụ thể, c&aacute;c chuy&ecirc;n gia của SgS sẽ c&oacute; chương tr&igrave;nh tập huấn hỗ trợ c&aacute;n bộ giảng vi&ecirc;n, c&aacute;c gi&aacute;o vi&ecirc;n hiểu về xu hướng, th&aacute;ch thức, lợi &iacute;ch v&agrave; nhu cầu của gi&aacute;o dục STEM; c&aacute;ch thức triển khai chương tr&igrave;nh học STEM tại trường dựa tr&ecirc;n y&ecirc;u cầu của chương tr&igrave;nh s&aacute;ch gi&aacute;o khoa; hiểu r&otilde; phương ph&aacute;p giảng dạy STEM qua c&aacute;c bước thiết kế b&agrave;i giảng, x&acirc;y dựng hoạt động STEM gắn liền với thực tế cuộc sống; c&aacute;ch thức lồng gh&eacute;p c&aacute;c kỹ năng nghi&ecirc;n cứu khoa học trong b&agrave;i giảng STEM; c&aacute;ch thức đ&aacute;nh gi&aacute; một b&agrave;i giảng v&agrave; tiết giảng STEM đạt ti&ecirc;u chuẩn.</span></p>
', N'giaoducstem09042019.JPG', 1, NULL, CAST(N'2019-04-09' AS Date), NULL)
INSERT [dbo].[Table_TinTuc] ([MaTinTuc], [TenTinTuc], [NoiDung], [HinhAnh], [NoiBat], [SoLuotXem], [created_at], [updated_at]) VALUES (N'TT007', N'Hội thảo khoa học “Nghiên cứu và dạy học Ngữ văn trong nhà trường hiện nay”', N'<p>
	<span style="box-sizing: border-box; font-weight: 700; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Ng&agrave;y 15/5/2018, trường Đại học Thủ Dầu Một đ&atilde; tổ chức th&agrave;nh c&ocirc;ng hội thảo khoa học với chủ đề &ldquo;Nghi&ecirc;n cứu v&agrave; dạy học Ngữ văn trong nh&agrave; trường hiện nay&rdquo;. Hội thảo đ&atilde; thu h&uacute;t sự quan t&acirc;m của nhiều nh&agrave; nghi&ecirc;n cứu, giảng vi&ecirc;n, học vi&ecirc;n v&agrave; sinh vi&ecirc;n đến từ nhiều cơ sở gi&aacute;o dục bậc đại học, Trung học phổ th&ocirc;ng v&agrave; Trung học cơ sở trong cả nước.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Hội thảo quy tụ 80 b&agrave;i tham luận của c&aacute;c nh&agrave; khoa học, c&aacute;c giảng vi&ecirc;n đại học, học vi&ecirc;n cao học v&agrave; nghi&ecirc;n cứu sinh đến nhiều cơ sở gi&aacute;o dục như: ĐH KHXH&amp;NV TPHCM, ĐH Sư phạm TP.HCM, Đại học S&agrave;i G&ograve;n, Viện Khoa học x&atilde; hội v&ugrave;ng Nam Bộ, Đại học Cần Thơ, Đại học T&acirc;y Đ&ocirc;, Đại học An Giang, Đại học Đồng Th&aacute;p, Đại học Bạc Li&ecirc;u, Cao đẳng Cộng đồng Hậu Giang, Đại học Quy Nhơn, Đại học Ph&uacute; Y&ecirc;n, Đại học Kh&aacute;nh H&ograve;a, Cao đẳng Quảng Ng&atilde;i, Nh&agrave; xuất bản Gi&aacute;o dục Việt Nam&hellip; Ngo&agrave;i ra, hội thảo cũng thu h&uacute;t nhiều tham luận đến từ c&aacute;c gi&aacute;o vi&ecirc;n THCS v&agrave; THPT, đội ngũ đang trực tiếp giảng dạy ở trường phổ th&ocirc;ng trong v&agrave; ngo&agrave;i Tỉnh như: Trường THCS- THPT Di&ecirc;n Hồng ở Đồng Th&aacute;p, THPT chuy&ecirc;n H&ugrave;ng Vương - B&igrave;nh Dương, THPT L&ecirc; Lợi ở B&igrave;nh Dương, THCS - THPT Nguyễn Khuyến, THPT Nguyễn Tất Th&agrave;nh, THPT Củ Chi - TPHCM, Trường THPT Long Hiệp - Tr&agrave; Vinh&hellip; Sau qu&aacute; tr&igrave;nh phản biện nghi&ecirc;m t&uacute;c, Ban tổ chức hội thảo đ&atilde; chọn được 64 b&agrave;i in kỷ yếu ch&iacute;nh thức của hội thảo.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Ph&aacute;t biểu khai mạc, PGS.TS Ho&agrave;ng Trọng Quyền &ndash; Ph&oacute; Hiệu trưởng ĐH Thủ Dầu Một nhấn mạnh, hoạt động nghi&ecirc;n cứu v&agrave; giảng dạy Ngữ văn trong nh&agrave; trường lu&ocirc;n l&agrave; vấn đề c&oacute; t&iacute;nh khoa học v&agrave; thực tiễn cấp thiết, nhất l&agrave; trong t&igrave;nh h&igrave;nh hiện nay, trước những đ&ograve;i hỏi mới của cuộc sống về gi&aacute;o dục nh&acirc;n c&aacute;ch, tư duy, phẩm chất con người Việt Nam đ&aacute;p ứng y&ecirc;u cầu ph&aacute;t triển v&agrave; hội nhập, v&agrave; trước những quan niệm tr&aacute;i chiều về việc kết cấu chương tr&igrave;nh ngữ văn đang được cả x&atilde; hội quan t&acirc;m, b&agrave;n thảo s&ocirc;i nổi. Ch&iacute;nh v&igrave; vậy, s&aacute;ng kiến tổ chức hội thảo lần n&agrave;y của trường Đại học Thủ Dầu l&agrave; nhằm l&agrave;m s&aacute;ng tỏ th&ecirc;m vấn đề đ&oacute;, đ&oacute;ng g&oacute;p những &yacute; kiến khoa học v&agrave; nh&acirc;n văn c&oacute; gi&aacute; trị ứng dụng v&agrave;o việc nghi&ecirc;n cứu v&agrave; giảng dạy ngữ văn trong nh&agrave; trường phổ th&ocirc;ng, cao đẳng v&agrave; đại học. Do vậy, hội thảo đ&atilde; nhận được sự quan t&acirc;m đặc biệt của nhiều đơin vị gi&aacute;o dục tr&ecirc;n cả nước.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Tại phi&ecirc;n to&agrave;n thể, hội thảo đ&atilde; lắng nghe tham luận &ldquo;</span><em style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">T&igrave;m hiểu chương tr&igrave;nh gi&aacute;o dục phổ th&ocirc;ng m&ocirc;n Ngữ văn (dự thảo) v&agrave; một số đề xuất, kiến nghị&rdquo;</em><span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">&nbsp;của TS. Phạm Thanh T&ugrave;ng, tham luận được tr&igrave;nh b&agrave;y với mục ti&ecirc;u mang t&iacute;nh chất g&oacute;p &yacute;, x&acirc;y dựng cho chương tr&igrave;nh ngữ văn ở phổ th&ocirc;ng; Tham luận &ldquo;Chương tr&igrave;nh gi&aacute;o dục Ph&aacute;p&rdquo; của TS. Sismondi, tham luận đ&atilde; tập trung ph&acirc;n t&iacute;ch đ&aacute;nh gi&aacute; chương tr&igrave;nh giảng dạy m&ocirc;n Ngữ văn trong nh&agrave; trường từ bậc trung học phổ th&ocirc;ng đến đại học của nước Ph&aacute;p, từ đ&oacute; đưa ra c&aacute;i nh&igrave;n đối s&aacute;ch giữa chương tr&igrave;nh gi&aacute;o dục phổ th&ocirc;ng ở Ph&aacute;p so với Việt Nam; Tham&nbsp;</span><em style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">luận &ldquo;Giảng dạy thơ ca cổ điển trong trường phổ th&ocirc;ng&rdquo;</em><span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">&nbsp;của ThS. Nguyễn Thị Bảo Anh, hướng đến c&aacute;c vấn đề như phương ph&aacute;p dạy thơ ca cổ điển trong trường phổ th&ocirc;ng, vai tr&ograve; của việc bồi dưỡng kỹ năng giảng dạy văn học cổ điển cho sinh vi&ecirc;n đại học v&agrave; gi&aacute; trị v&agrave; &yacute; nghĩa của chữ H&aacute;n, chữ N&ocirc;m đối với sự tinh luyện tiếng Việt.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Đi s&acirc;u v&agrave;o b&agrave;n luận, hội thảo chia th&agrave;nh c&aacute;c tiểu ban với những mảng đề t&agrave;i li&ecirc;n quan như Nghi&ecirc;n cứu v&agrave; giảng dạy trong cao đẳng, đại học; Nghi&ecirc;n cứu v&agrave; giảng dạy trong nh&agrave; trường phổ th&ocirc;ng; Nghi&ecirc;n cứu văn chương v&agrave; nghi&ecirc;n cứu ng&ocirc;n ngữ. Trong khu&ocirc;n khổ hội thảo, c&aacute;c đại biểu đ&atilde; thảo luận những vấn đề li&ecirc;n quan về một số t&aacute;c giả, t&aacute;c phẩm v&agrave; thể loại văn học giảng dạy trong trường phổ th&ocirc;ng; vấn đề ng&ocirc;n ngữ v&agrave; định hướng nghi&ecirc;n cứu trong lĩnh vực n&agrave;y; giải ph&aacute;p v&agrave; biện ph&aacute;p nhằm ph&aacute;t triển phẩm chất v&agrave; năng lực cho học sinh, sinh vi&ecirc;n; kinh nghiệm giải quyết c&aacute;c t&igrave;nh huống sư phạm trong giảng dạy Ngữ văn ở nh&agrave; trường&hellip;</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Đ&uacute;c kết tổng luận về hội thảo, TS. H&agrave; Thanh V&acirc;n - Ph&oacute; trưởng khoa Ngữ văn, ĐH Thủ Dầu Một đề cao tinh thần l&agrave;m việc khẩn trương, nghi&ecirc;m t&uacute;c, khoa học của c&aacute;c đại biểu. C&aacute;c tham luận đ&atilde; l&agrave;m s&aacute;ng tỏ nhiều vấn đề m&agrave; hội thảo đặt ra. Trong đ&oacute;, c&oacute; những nội dung được đ&agrave;o s&acirc;u nghi&ecirc;n cứu, v&agrave; cũng c&oacute; những vấn đề dừng ở g&oacute;c nh&igrave;n tổng qu&aacute;t hoặc l&agrave; những gợi &yacute; để tiếp tục nghi&ecirc;n cứu. C&aacute;c kết quả nghi&ecirc;n cứu từ hội thảo đ&atilde; g&oacute;p phần gi&uacute;p cho c&aacute;c nh&agrave; nghi&ecirc;n cứu, gi&aacute;o vi&ecirc;n, sinh vi&ecirc;n,&hellip; c&oacute; th&ecirc;m kinh nghiệm trong nghi&ecirc;n cứu văn học, ng&ocirc;n ngữ v&agrave; hoạt động dạy - học m&ocirc;n Ngữ văn sẽ đ&aacute;p ứng tốt hơn mục ti&ecirc;u, y&ecirc;u cầu đổi mới của chương tr&igrave;nh gi&aacute;o dục phổ th&ocirc;ng hiện nay.</span></p>
', N'nghiencuunguvan09042019.JPG', 1, 1, CAST(N'2019-04-09' AS Date), NULL)
INSERT [dbo].[Table_TinTuc] ([MaTinTuc], [TenTinTuc], [NoiDung], [HinhAnh], [NoiBat], [SoLuotXem], [created_at], [updated_at]) VALUES (N'TT008', N'Báo cáo chuyên đề “Nâng cao chất lượng đề cương nghiên cứu thông qua bình duyệt”', N'<p>
	<span style="box-sizing: border-box; font-weight: 700; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Ng&agrave;y 13/6/2018, trường Đại học Thủ Dầu Một đ&atilde; phối hợp với Trung t&acirc;m Nghi&ecirc;n cứu Việt Nam v&agrave; Đ&ocirc;ng Nam &Aacute; &ndash; ĐH KHXH&amp;NV (ĐHQG Tp.HCM) tổ chức buổi b&aacute;o c&aacute;o chuy&ecirc;n đề &ldquo;N&acirc;ng cao chất lượng đề cương nghi&ecirc;n cứu th&ocirc;ng qua b&igrave;nh duyệt&rdquo;. Chương tr&igrave;nh thuộc dự &aacute;n &ldquo;N&acirc;ng cao năng lực nghi&ecirc;n cứu khoa học v&agrave; đổi mới tại Việt Nam&rdquo;.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Tham dự buổi b&aacute;o c&aacute;o c&oacute; PGS.TS Ho&agrave;ng Trọng Quyền &ndash; Ph&oacute; Hiệu trưởng, c&ugrave;ng l&atilde;nh đạo c&aacute;c ph&ograve;ng, ban, khoa v&agrave; hơn 100 CBGV của Trường. Ph&aacute;t biểu tại buổi b&aacute;o c&aacute;o, TS. Trần Đ&igrave;nh L&acirc;m &ndash; Gi&aacute;m đốc Trung t&acirc;m Nghi&ecirc;n cứu Việt Nam v&agrave; Đ&ocirc;ng Nam &Aacute; cho biết, dự &aacute;n ENHANCE (t&ecirc;n đầy đủ: Trengthening national research and innovation capacities in Vietnam - N&acirc;ng cao năng lực nghi&ecirc;n cứu khoa học v&agrave; đổi mới tại Việt Nam). Dự &aacute;n n&agrave;y được điều phối bởi trường Đại học Alicante - T&acirc;y Ban Nha, trường Đại học Glasgow Caledonian - Scotland v&agrave; trường Đại học C&ocirc;ng nghệ Slovak - Slovakia. C&aacute;c th&agrave;nh vi&ecirc;n tại Việt Nam bao gồm: Đại học KHXH&amp;NV Tp. HCM, Đại học KHXH&amp;NV H&agrave; Nội, Đại học N&ocirc;ng L&acirc;m Huế, Đại học Th&aacute;i Nguy&ecirc;n, Đại học Cần Thơ v&agrave; Đại học An Giang. C&aacute;c th&agrave;nh vi&ecirc;n của dự &aacute;n sẽ đảm nhiệm ch&iacute;nh c&aacute;c g&oacute;i c&ocirc;ng việc, hoạt động kh&aacute;c nhau, nhằm g&oacute;p phần th&uacute;c đẩy v&agrave; n&acirc;ng cao năng lực nghi&ecirc;n cứu khoa học, đổi mới trong chuyển giao c&ocirc;ng nghệ, chuyển giao kiến thức tại c&aacute;c trường đại học, cao đẳng v&agrave; c&aacute;c viện nghi&ecirc;n cứu của Việt Nam. Th&ocirc;ng qua hoạt động học thuật &yacute; nghĩa n&agrave;y, TS. Trần Đ&igrave;nh L&acirc;m hy vọng sẽ g&oacute;p phần gi&uacute;p cho CBGV trường Đại học Thủ Dầu Một c&oacute; th&ecirc;m nhiều kiến thức v&agrave; phương ph&aacute;p để triển khai đề t&agrave;i nghi&ecirc;n cứu khoa học một c&aacute;ch hiệu quả v&agrave; theo đ&uacute;ng ti&ecirc;u chuẩn APA quốc tế (The American Psychological Association).</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Điều phối nội dung của buổi b&aacute;o c&aacute;o chuy&ecirc;n đề, ThS. Văn Thị Nh&atilde; Tr&uacute;c &ndash; Giảng vi&ecirc;n trường ĐH KHXH&amp;NV Tp.HCM đ&atilde; cung cấp nhiều th&ocirc;ng tin hữu &iacute;ch về cấu tr&uacute;c của một đề cương nghi&ecirc;n cứu, phương ph&aacute;p v&agrave; c&aacute;c kỹ năng nghi&ecirc;n cứu, th&aacute;i độ v&agrave; phẩm chất của nh&agrave; khoa học, những điều kiện cần cho việc ph&aacute;t triển năng lực nghi&ecirc;n cứu. B&ecirc;n cạnh việc trao đổi kiến thức v&agrave; kỹ năng nghi&ecirc;n cứu khoa học, ThS. Văn Thị Nh&atilde; Tr&uacute;c cũng d&agrave;nh nhiều thời gian thảo luận, chia sẻ kinh nghiệm v&agrave; trả lời c&aacute;c c&acirc;u hỏi li&ecirc;n quan đến c&aacute;ch thức viết đề cương nghi&ecirc;n cứu xin học bổng nước ngo&agrave;i, kỹ năng triển khai viết một b&agrave;i b&aacute;o c&aacute;o khoa học đạt y&ecirc;u cầu của c&aacute;c tạp ch&iacute; khoa học trong nước v&agrave; quốc tế.</span></p>
', N'trinhduyet09042019.JPG', 1, 1, CAST(N'2019-04-09' AS Date), NULL)
INSERT [dbo].[Table_TinTuc] ([MaTinTuc], [TenTinTuc], [NoiDung], [HinhAnh], [NoiBat], [SoLuotXem], [created_at], [updated_at]) VALUES (N'TT009', N'Học hỏi và chia sẻ kinh nghiệm thực hiện CDIO', N'<p>
	<span style="box-sizing: border-box; font-weight: 700; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Trong tuần qua, Đại học Thủ Dầu Một đ&atilde; tham gia c&aacute;c hoạt động đ&uacute;c kết, chia sẻ kinh nghiệm triển khai CDIO tại Hội nghị Tổng kết đề &aacute;n CDIO của Đại học Quốc gia Tp.HCM v&agrave; với Tổ chức Gi&aacute;o dục Hoa Kỳ (IAE).</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="box-sizing: border-box; font-weight: 700; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Tr&igrave;nh b&agrave;y tham luận tại Hội nghị Tổng kết đề &aacute;n CDIO giai đoạn 2010-2017</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">S&aacute;ng ng&agrave;y 5/12/2017, Đo&agrave;n c&aacute;n bộ trường ĐH Thủ Dầu Một do TS.Ng&ocirc; Hồng Điệp &ndash; Ph&oacute; Hiệu trưởng l&agrave;m trưởng đo&agrave;n đ&atilde; tham dự Hội nghị Tổng kết đề &aacute;n CDIO giai đoạn 2010 &ndash; 2017 do ĐHQG Tp.HCM tổ chức. Hội nghị c&oacute; sự tham dự, chủ tr&igrave; của PGS.TS Huỳnh Th&agrave;nh Đạt &ndash; Gi&aacute;m đốc ĐHQG Tp.HCM, c&ugrave;ng l&atilde;nh đạo c&aacute;c trường th&agrave;nh vi&ecirc;n, c&aacute;c trường đại học đang triển khai đề xướng CDIO.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Trong 5 tham luận tr&igrave;nh b&agrave;y tại hội nghị, Đại học Thủ Dầu Một l&agrave; trường duy nhất ngo&agrave;i khối ĐHQG được mời b&aacute;o c&aacute;o về &ldquo;N&acirc;ng cao năng lực giảng dạy của giảng vi&ecirc;n Trường Đại học Thủ Dầu Một th&ocirc;ng qua chiến lược đ&agrave;o tạo bồi dưỡng thường xuy&ecirc;n tại Trường&rdquo;. Đại diện ĐH Thủ Dầu Một, PGS.TS Nguyễn Đức Lộc &ndash; Viện trưởng Viện Chiến lược đ&atilde; tr&igrave;nh b&agrave;y những nội dung trọng t&acirc;m của tham luận về triết l&yacute; gi&aacute;o dục h&ograve;a hợp t&iacute;ch cực dựa tr&ecirc;n nguy&ecirc;n tắc lấy việc học l&agrave;m trung t&acirc;m, th&ocirc;ng qua việc &aacute;p dụng c&aacute;c c&ocirc;ng cụ n&acirc;ng cao năng lực cho giảng vi&ecirc;n như ISW (kỹ năng giảng dạy), FDW (kỹ năng điều phối), ADW (kỹ năng đ&aacute;nh gi&aacute;), OnCDW (thiết kế kh&oacute;a học trực tuyến),&hellip; Những chương tr&igrave;nh m&agrave; Trường đang triển khai nhằm đ&aacute;p ứng hiệu quả c&aacute;c ti&ecirc;u chuẩn của CDIO đặc biệt l&agrave; ti&ecirc;u chuẩn 8, 9, 10.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Trong ph&aacute;t biểu kết luận hội nghị, PGS.TS Nguyễn Hội Nghĩa &ndash; Ph&oacute; Gi&aacute;m đốc ĐHQG Tp.HCM khẳng định những hiệu quả thiết thực m&agrave; CDIO mang lại trong cải tiến chất lượng của c&aacute;c Trường. Hội nghị thống nhất tiếp tục triển khai đề &aacute;n CDIO trong điều kiện mới, gắn với cuộc c&aacute;ch mạng c&ocirc;ng nghiệp 4.0.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="box-sizing: border-box; font-weight: 700; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Chia sẻ kinh nghiệm triển khai CDIO với&nbsp;</span><span style="box-sizing: border-box; font-weight: 700; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Tổ chức Gi&aacute;o dục Hoa Kỳ (IAE)</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">S&aacute;ng 6/12/2017, c&aacute;c th&agrave;nh vi&ecirc;n thuộc Tổ chức Gi&aacute;o dục Hoa Kỳ (IAE) do TS Ho&agrave;ng Anh Tuấn Kiệt - Ph&oacute; Hiệu trưởng Đại học Th&agrave;nh T&acirc;y c&ugrave;ng l&atilde;nh đạo c&aacute;c trường trực thuộc IAE như Cao Đẳng Việt Mỹ, Trung cấp B&aacute;ch Khoa,&hellip; đ&atilde; đến l&agrave;m việc với Đại học Thủ Dầu Một. Tiếp đo&agrave;n, c&oacute; TS Ng&ocirc; Hồng Điệp &ndash; Ph&oacute; Hiệu trưởng, l&atilde;nh đạo Viện Chiến lược, ph&ograve;ng Đảm bảo chất lượng, ph&ograve;ng Đ&agrave;o tạo Đại học, khoa Khoa học C&ocirc;ng nghệ c&ugrave;ng c&aacute;c chuy&ecirc;n gia về triển khai CDIO tại Trường.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Ph&aacute;t biểu về mục ti&ecirc;u của buổi l&agrave;m việc, TS Anh Kiệt cho biết Tổ chức gi&aacute;o dục Hoa Kỳ nghi&ecirc;m t&uacute;c xem việc ph&aacute;t triển chương tr&igrave;nh theo định hướng CDIO l&agrave; kim chỉ nam mang t&iacute;nh chất sống c&ograve;n của đơn vị. Do vậy, c&aacute;c th&agrave;nh vi&ecirc;n của IAE đ&atilde; cử một nh&oacute;m c&aacute;n bộ tập trung học hỏi v&agrave; t&igrave;m hiểu về c&aacute;ch thức vận dụng CDIO từ c&aacute;c Trường đ&atilde; thực hiện th&agrave;nh c&ocirc;ng. Trong đ&oacute;, ĐH Thủ Dầu Một &ndash; th&agrave;nh vi&ecirc;n của tổ chức CDIO thế giới từ năm 2015 - l&agrave; một trong những Trường g&acirc;y ấn tượng với sự ph&aacute;t triển nhanh, mạnh, đặc biệt &aacute;p dụng hiệu quả CDIO trong qu&aacute; tr&igrave;nh ph&aacute;t triển, cũng như những hỗ trợ nhiệt t&igrave;nh cho c&aacute;c đơn vị kh&aacute;c.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Đ&aacute;p lại sự tin tưởng của IAE, PGS.TS L&ecirc; Tuấn Anh đ&atilde; thay mặt c&aacute;c chuy&ecirc;n gia thực hiện CDIO tr&igrave;nh b&agrave;y những kinh nghiệm triển khai CDIO tại Trường. Theo đ&oacute;, CDIO l&agrave; một c&ocirc;ng cụ quan trọng đ&aacute;p ứng mục ti&ecirc;u trọng t&acirc;m l&agrave; kiểm định, thực hiện cam kết cốt l&otilde;i l&agrave; đảm bảo chất lượng. Trong c&aacute;ch đưa CDIO v&agrave;o thực tiễn vận h&agrave;nh, Trường đi bằng cả 2 hướng c&ugrave;ng l&uacute;c: từ tr&ecirc;n xuống v&agrave; từ dưới l&ecirc;n, cụ thể l&agrave; từ sự quyết liệt thực hiện của l&atilde;nh đạo v&agrave; từ sự thấu hiểu, đồng l&ograve;ng của đội ngũ giảng vi&ecirc;n.</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Qua qu&aacute; tr&igrave;nh thử nghiệm, &aacute;p dụng, cải tiến, Trường đ&atilde; chắt lọc v&agrave; x&aacute;c định hai phương ph&aacute;p hiệu quả hiện nay nhằm đ&aacute;p ứng c&aacute;c ti&ecirc;u chuẩn của CDIO l&agrave; E-learning v&agrave; c&aacute;c chương tr&igrave;nh n&acirc;ng cao tinh thần/ kỹ năng giảng dạy của giảng vi&ecirc;n. Sau khi t&igrave;m hiểu v&agrave; trao đổi về 2 phương ph&aacute;p n&agrave;y, c&aacute;c th&agrave;nh vi&ecirc;n IAE mong muốn ĐH Thủ Dầu Một sẽ chuyển giao c&ocirc;ng nghệ hoặc đ&agrave;o tạo cho IAE c&aacute;c m&ocirc;n về Tư tưởng dựa tr&ecirc;n c&ocirc;ng cụ E-learning, tổ chức c&aacute;c kh&oacute;a tập huấn ISW, FDW, ADW,&hellip; cho giảng vi&ecirc;n IAE .</span><br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<br style="box-sizing: border-box; color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;" />
	<span style="color: rgb(51, 51, 51); font-family: sans-serif; font-size: 14px; text-align: justify;">Kết th&uacute;c buổi l&agrave;m việc, đại diện IAE mong muốn l&atilde;nh đạo Trường sẽ ĐH Thủ Dầu Một sẽ chấp thuận những để nghị của IAE, sẵn s&agrave;ng hợp t&aacute;c v&agrave; hỗ trợ IAE trong qu&aacute; tr&igrave;nh &aacute;p dụng CDIO.&nbsp;</span></p>
', N'cdio09042019.jpg', 1, 6, CAST(N'2019-04-09' AS Date), NULL)
INSERT [dbo].[Table_TrinhDoHocVan] ([MaTDHV], [TenTDHV], [created_at], [updated_at]) VALUES (N'TDHT02', N'Thạc Sĩ', CAST(N'2019-02-16' AS Date), NULL)
INSERT [dbo].[Table_TrinhDoHocVan] ([MaTDHV], [TenTDHV], [created_at], [updated_at]) VALUES (N'TDHV01', N'Tiến Sĩ', CAST(N'2018-11-10' AS Date), NULL)
INSERT [dbo].[Table_User] ([MaUser], [TaiKhoan], [MatKhau], [CapDo], [created_at], [updated_at], [NhanVien_MNV]) VALUES (N'US01', N'demoad', N'$2a$12$wI5DZO5qA.3.e15UATV46e.YCKjMmkIHp.br9kGobPNQnfLiS9mQa', 1, CAST(N'2018-11-23' AS Date), CAST(N'2019-04-09' AS Date), N'NV001')
INSERT [dbo].[Table_User] ([MaUser], [TaiKhoan], [MatKhau], [CapDo], [created_at], [updated_at], [NhanVien_MNV]) VALUES (N'US02', N'demo', N'$2a$12$7nhIQ4OKxL331VjrmcKQ3uJeE7zKLXzYKkYlnzI1LWiYvBeNty.72', 0, CAST(N'2018-11-23' AS Date), CAST(N'2019-04-09' AS Date), N'NV003')
INSERT [dbo].[Table_User] ([MaUser], [TaiKhoan], [MatKhau], [CapDo], [created_at], [updated_at], [NhanVien_MNV]) VALUES (N'US03', N'demo02', N'$2a$12$8ixb8vxbA1MyoGNfB5wKiupDnXKQOfk3LiyMR5igy58cwQzAIXL1m', 0, CAST(N'2018-11-24' AS Date), NULL, N'NV002')
INSERT [dbo].[Table_User] ([MaUser], [TaiKhoan], [MatKhau], [CapDo], [created_at], [updated_at], [NhanVien_MNV]) VALUES (N'US04', N'demo03', N'$2a$12$6KCls4ExuvoIVXi1pJ3wje52XWxXJTGsTWU6MKMQIGlJBy/Y3fVTa', 0, CAST(N'2019-01-19' AS Date), NULL, N'NV005')
INSERT [dbo].[Table_User] ([MaUser], [TaiKhoan], [MatKhau], [CapDo], [created_at], [updated_at], [NhanVien_MNV]) VALUES (N'US05', N'demo04', N'$2a$12$ZvmeiaLzNSCq1Pwvs59AQ.GJvcpMysn8cZOpF6IqKwPuLxCxkhhCS', 0, CAST(N'2019-04-07' AS Date), NULL, N'NV004')
INSERT [dbo].[Table_User] ([MaUser], [TaiKhoan], [MatKhau], [CapDo], [created_at], [updated_at], [NhanVien_MNV]) VALUES (N'US06', N'kiemduyet', N'$2a$12$sa1ed59i42ULPeJYUT3KU.JtVPzxwPxmsFSJKrcmnjzuS5oEuAZeG', 2, CAST(N'2019-05-10' AS Date), NULL, N'NV004')
SET IDENTITY_INSERT [dbo].[Table_VanBanTDKT] ON 

INSERT [dbo].[Table_VanBanTDKT] ([MaVanBan], [TenVanBan], [Link], [NgayBanHanh], [created_at], [updated_at], [LoaiHinh_MLH]) VALUES (1, N'Thông báo Kế hoạch thi tuyển chức danh lãnh đạo, quản lý cấp vụ và tương đương tại một số vụ, đơn vị thuộc Ban Thi đua - Khen thưởng Trung ương', N'https://drive.google.com/open?id=1bJuaci6Wsrw9mW4hrXTkcS0Xmw0aqTca', CAST(N'2019-02-05' AS Date), CAST(N'2019-02-05' AS Date), CAST(N'2019-02-06' AS Date), N'LH01')
INSERT [dbo].[Table_VanBanTDKT] ([MaVanBan], [TenVanBan], [Link], [NgayBanHanh], [created_at], [updated_at], [LoaiHinh_MLH]) VALUES (2, N'Danh sách 70 điển hình tiên tiến tuyên dương tại Lễ kỷ niệm 70 năm Ngày Chủ tịch Hồ Chí Minh ra Lời kêu gọi Thi đua ái quốc', N'https://drive.google.com/open?id=1x3uD3r2zPBQrE4HgZGN0lp8ENp-iR7FX', CAST(N'2019-02-03' AS Date), CAST(N'2019-02-05' AS Date), CAST(N'2019-02-06' AS Date), N'LH02')
INSERT [dbo].[Table_VanBanTDKT] ([MaVanBan], [TenVanBan], [Link], [NgayBanHanh], [created_at], [updated_at], [LoaiHinh_MLH]) VALUES (3, N'Quyết định số 346/QĐ-TTg về việc Phê duyệt Kế hoạch tổ chức hoạt động kỷ niệm 70 năm Ngày Chủ tịch Hồ Chí Minh ra lời kêu gọi thi đua ái quốc (11/6/1948 - 11/6/2018)', N'https://drive.google.com/open?id=1EnbgksSnMe5YnBqFTeFGAal40gjQRQLt', CAST(N'2018-05-07' AS Date), CAST(N'2019-02-05' AS Date), CAST(N'2019-02-06' AS Date), N'LH01')
INSERT [dbo].[Table_VanBanTDKT] ([MaVanBan], [TenVanBan], [Link], [NgayBanHanh], [created_at], [updated_at], [LoaiHinh_MLH]) VALUES (4, N'Quyết định 318/QĐ-BNV về việc công bố thủ tục hành chính mới ban hành trong lĩnh vực thi đua, khen thưởng thuộc phạm vi chức năng quản lý của Bộ Nội vụ', N'https://drive.google.com/open?id=1J4hZ2RgEKs3J5kDFKWzw0jbVbD7jpzUE', CAST(N'2017-06-20' AS Date), CAST(N'2019-02-05' AS Date), CAST(N'2019-02-06' AS Date), N'LH03')
INSERT [dbo].[Table_VanBanTDKT] ([MaVanBan], [TenVanBan], [Link], [NgayBanHanh], [created_at], [updated_at], [LoaiHinh_MLH]) VALUES (5, N'Chỉ thị số 22-CT/TW của Ban Bí thư về việc đẩy mạnh các phong trào thi đua yêu nước hướng tới kỷ niệm 70 năm Ngày Chủ tịch Hồ Chí Minh ra Lời kêu gọi thi đua ái quốc (11/6/1948-11/6/2018)', N'https://drive.google.com/open?id=1x0v4_-a5VTgOnE6Euhjrm5_41rrLEH5A', CAST(N'2016-06-22' AS Date), CAST(N'2019-02-05' AS Date), CAST(N'2019-02-06' AS Date), N'LH04')
INSERT [dbo].[Table_VanBanTDKT] ([MaVanBan], [TenVanBan], [Link], [NgayBanHanh], [created_at], [updated_at], [LoaiHinh_MLH]) VALUES (6, N'Nghị định 91/2017/NĐ-CP quy định chi tiết thi hành một số điều Luật Thi đua, Khen thưởng', N'https://drive.google.com/open?id=10HeNShIX_HDZ0KTFnet4ESME8j9K_J4m', CAST(N'2018-08-15' AS Date), CAST(N'2019-02-05' AS Date), CAST(N'2019-02-06' AS Date), N'LH05')
INSERT [dbo].[Table_VanBanTDKT] ([MaVanBan], [TenVanBan], [Link], [NgayBanHanh], [created_at], [updated_at], [LoaiHinh_MLH]) VALUES (7, N'Chỉ thị 18/CT-TTg ngày 19/5/2016 của Thủ tướng Chính phủ', N'https://drive.google.com/open?id=1Uexh8ti-DwUC7vMzyegINV8sFASLJBW9', CAST(N'2019-02-04' AS Date), CAST(N'2019-02-05' AS Date), CAST(N'2019-02-06' AS Date), N'LH04')
INSERT [dbo].[Table_VanBanTDKT] ([MaVanBan], [TenVanBan], [Link], [NgayBanHanh], [created_at], [updated_at], [LoaiHinh_MLH]) VALUES (8, N'Chỉ thị 18/CT-TTg ngày 19/5/2016 của Thủ tướng Chính phủ', N'https://drive.google.com/open?id=1oqoq43Z93UM32v2FzcD7jLWrL82afj59', CAST(N'2019-02-14' AS Date), CAST(N'2019-02-05' AS Date), CAST(N'2019-02-06' AS Date), N'LH04')
SET IDENTITY_INSERT [dbo].[Table_VanBanTDKT] OFF
ALTER TABLE [dbo].[Table_BaiBao]  WITH CHECK ADD  CONSTRAINT [FK_Table_BaiBao_Table_NamHoc] FOREIGN KEY([NamHoc_MNH])
REFERENCES [dbo].[Table_NamHoc] ([MaNamHoc])
GO
ALTER TABLE [dbo].[Table_BaiBao] CHECK CONSTRAINT [FK_Table_BaiBao_Table_NamHoc]
GO
ALTER TABLE [dbo].[Table_BaiBao]  WITH CHECK ADD  CONSTRAINT [FK_Table_BaiBao_Table_NhanVien] FOREIGN KEY([NhanVien_MNV])
REFERENCES [dbo].[Table_NhanVien] ([MaNhanVien])
GO
ALTER TABLE [dbo].[Table_BaiBao] CHECK CONSTRAINT [FK_Table_BaiBao_Table_NhanVien]
GO
ALTER TABLE [dbo].[Table_Comment]  WITH CHECK ADD  CONSTRAINT [FK_Table_Comment_Table_SuKien] FOREIGN KEY([SuKien_id])
REFERENCES [dbo].[Table_SuKien] ([MaSuKien])
GO
ALTER TABLE [dbo].[Table_Comment] CHECK CONSTRAINT [FK_Table_Comment_Table_SuKien]
GO
ALTER TABLE [dbo].[Table_Comment]  WITH CHECK ADD  CONSTRAINT [FK_Table_Comment_Table_TinTuc] FOREIGN KEY([TinTuc_id])
REFERENCES [dbo].[Table_TinTuc] ([MaTinTuc])
GO
ALTER TABLE [dbo].[Table_Comment] CHECK CONSTRAINT [FK_Table_Comment_Table_TinTuc]
GO
ALTER TABLE [dbo].[Table_Comment]  WITH CHECK ADD  CONSTRAINT [FK_Table_Comment_Table_User] FOREIGN KEY([User_id])
REFERENCES [dbo].[Table_User] ([MaUser])
GO
ALTER TABLE [dbo].[Table_Comment] CHECK CONSTRAINT [FK_Table_Comment_Table_User]
GO
ALTER TABLE [dbo].[Table_DeNghiTangLuong]  WITH CHECK ADD  CONSTRAINT [FK_Table_DeNghiTangLuong_Table_KhenThuong] FOREIGN KEY([KhenThuong_MKT])
REFERENCES [dbo].[Table_KhenThuong] ([MaKhenThuong])
GO
ALTER TABLE [dbo].[Table_DeNghiTangLuong] CHECK CONSTRAINT [FK_Table_DeNghiTangLuong_Table_KhenThuong]
GO
ALTER TABLE [dbo].[Table_DeNghiTangLuong]  WITH CHECK ADD  CONSTRAINT [FK_Table_DeNghiTangLuong_Table_NhanVien] FOREIGN KEY([NhanVien_MNV])
REFERENCES [dbo].[Table_NhanVien] ([MaNhanVien])
GO
ALTER TABLE [dbo].[Table_DeNghiTangLuong] CHECK CONSTRAINT [FK_Table_DeNghiTangLuong_Table_NhanVien]
GO
ALTER TABLE [dbo].[Table_DeTaiNCKH]  WITH CHECK ADD  CONSTRAINT [FK_Table_DeTaiNCKH_Table_NamHoc] FOREIGN KEY([NamHoc_MNH])
REFERENCES [dbo].[Table_NamHoc] ([MaNamHoc])
GO
ALTER TABLE [dbo].[Table_DeTaiNCKH] CHECK CONSTRAINT [FK_Table_DeTaiNCKH_Table_NamHoc]
GO
ALTER TABLE [dbo].[Table_DeTaiNCKH]  WITH CHECK ADD  CONSTRAINT [FK_Table_DeTaiNCKH_Table_NhanVien] FOREIGN KEY([NhanVien_MNV])
REFERENCES [dbo].[Table_NhanVien] ([MaNhanVien])
GO
ALTER TABLE [dbo].[Table_DeTaiNCKH] CHECK CONSTRAINT [FK_Table_DeTaiNCKH_Table_NhanVien]
GO
ALTER TABLE [dbo].[Table_DeXuatKhenThuong]  WITH CHECK ADD  CONSTRAINT [FK_Table_DeXuatKhenThuong_Table_NamHoc] FOREIGN KEY([NamHoc_MNH])
REFERENCES [dbo].[Table_NamHoc] ([MaNamHoc])
GO
ALTER TABLE [dbo].[Table_DeXuatKhenThuong] CHECK CONSTRAINT [FK_Table_DeXuatKhenThuong_Table_NamHoc]
GO
ALTER TABLE [dbo].[Table_DeXuatKhenThuong]  WITH CHECK ADD  CONSTRAINT [FK_Table_DeXuatKhenThuong_Table_NhanVien] FOREIGN KEY([NhanVien_MNV])
REFERENCES [dbo].[Table_NhanVien] ([MaNhanVien])
GO
ALTER TABLE [dbo].[Table_DeXuatKhenThuong] CHECK CONSTRAINT [FK_Table_DeXuatKhenThuong_Table_NhanVien]
GO
ALTER TABLE [dbo].[Table_DeXuatKhenThuong]  WITH CHECK ADD  CONSTRAINT [FK_Table_DeXuatKhenThuong_Table_PhongBan] FOREIGN KEY([PhongBan_MPB])
REFERENCES [dbo].[Table_PhongBan] ([MaPhongBan])
GO
ALTER TABLE [dbo].[Table_DeXuatKhenThuong] CHECK CONSTRAINT [FK_Table_DeXuatKhenThuong_Table_PhongBan]
GO
ALTER TABLE [dbo].[Table_DiemDuNamTruoc]  WITH CHECK ADD  CONSTRAINT [FK_Table_DiemDuNamTruoc_Table_NamHoc] FOREIGN KEY([MaNamHoc])
REFERENCES [dbo].[Table_NamHoc] ([MaNamHoc])
GO
ALTER TABLE [dbo].[Table_DiemDuNamTruoc] CHECK CONSTRAINT [FK_Table_DiemDuNamTruoc_Table_NamHoc]
GO
ALTER TABLE [dbo].[Table_DiemDuNamTruoc]  WITH CHECK ADD  CONSTRAINT [FK_Table_DiemDuNamTruoc_Table_NhanVien] FOREIGN KEY([MaNhanVien])
REFERENCES [dbo].[Table_NhanVien] ([MaNhanVien])
GO
ALTER TABLE [dbo].[Table_DiemDuNamTruoc] CHECK CONSTRAINT [FK_Table_DiemDuNamTruoc_Table_NhanVien]
GO
ALTER TABLE [dbo].[Table_GioCongTac]  WITH CHECK ADD  CONSTRAINT [FK_Table_GioCongTac_Table_NamHoc] FOREIGN KEY([NamHoc_MNH])
REFERENCES [dbo].[Table_NamHoc] ([MaNamHoc])
GO
ALTER TABLE [dbo].[Table_GioCongTac] CHECK CONSTRAINT [FK_Table_GioCongTac_Table_NamHoc]
GO
ALTER TABLE [dbo].[Table_GioCongTac]  WITH CHECK ADD  CONSTRAINT [FK_Table_GioCongTac_Table_NhanVien] FOREIGN KEY([NhanVien_MNV])
REFERENCES [dbo].[Table_NhanVien] ([MaNhanVien])
GO
ALTER TABLE [dbo].[Table_GioCongTac] CHECK CONSTRAINT [FK_Table_GioCongTac_Table_NhanVien]
GO
ALTER TABLE [dbo].[Table_KhenThuong]  WITH CHECK ADD  CONSTRAINT [FK_Table_KhenThuong_Table_CapKhen] FOREIGN KEY([CapKhen_MCK])
REFERENCES [dbo].[Table_CapKhen] ([MaCapKhen])
GO
ALTER TABLE [dbo].[Table_KhenThuong] CHECK CONSTRAINT [FK_Table_KhenThuong_Table_CapKhen]
GO
ALTER TABLE [dbo].[Table_KhenThuong]  WITH CHECK ADD  CONSTRAINT [FK_Table_KhenThuong_Table_DanhHieu] FOREIGN KEY([DanhHieu_MDH])
REFERENCES [dbo].[Table_DanhHieu] ([MaDanhHieu])
GO
ALTER TABLE [dbo].[Table_KhenThuong] CHECK CONSTRAINT [FK_Table_KhenThuong_Table_DanhHieu]
GO
ALTER TABLE [dbo].[Table_KhenThuong]  WITH CHECK ADD  CONSTRAINT [FK_Table_KhenThuong_Table_HinhThucKhenThuong] FOREIGN KEY([HinhThucKhenThuong_MHT])
REFERENCES [dbo].[Table_HinhThucKhenThuong] ([MaHinhThucKhenThuong])
GO
ALTER TABLE [dbo].[Table_KhenThuong] CHECK CONSTRAINT [FK_Table_KhenThuong_Table_HinhThucKhenThuong]
GO
ALTER TABLE [dbo].[Table_KhenThuong]  WITH CHECK ADD  CONSTRAINT [FK_Table_KhenThuong_Table_NhanVien] FOREIGN KEY([NhanVien_MNV])
REFERENCES [dbo].[Table_NhanVien] ([MaNhanVien])
GO
ALTER TABLE [dbo].[Table_KhenThuong] CHECK CONSTRAINT [FK_Table_KhenThuong_Table_NhanVien]
GO
ALTER TABLE [dbo].[Table_KhenThuongTapThe]  WITH CHECK ADD  CONSTRAINT [FK_Table_KhenThuongTapThe_Table_CapKhen] FOREIGN KEY([CapKhen_MCK])
REFERENCES [dbo].[Table_CapKhen] ([MaCapKhen])
GO
ALTER TABLE [dbo].[Table_KhenThuongTapThe] CHECK CONSTRAINT [FK_Table_KhenThuongTapThe_Table_CapKhen]
GO
ALTER TABLE [dbo].[Table_KhenThuongTapThe]  WITH CHECK ADD  CONSTRAINT [FK_Table_KhenThuongTapThe_Table_DanhHieu] FOREIGN KEY([DanhHieu_MDH])
REFERENCES [dbo].[Table_DanhHieu] ([MaDanhHieu])
GO
ALTER TABLE [dbo].[Table_KhenThuongTapThe] CHECK CONSTRAINT [FK_Table_KhenThuongTapThe_Table_DanhHieu]
GO
ALTER TABLE [dbo].[Table_KhenThuongTapThe]  WITH CHECK ADD  CONSTRAINT [FK_Table_KhenThuongTapThe_Table_HinhThucKhenThuong] FOREIGN KEY([HinhThucKhenThuong_MHT])
REFERENCES [dbo].[Table_HinhThucKhenThuong] ([MaHinhThucKhenThuong])
GO
ALTER TABLE [dbo].[Table_KhenThuongTapThe] CHECK CONSTRAINT [FK_Table_KhenThuongTapThe_Table_HinhThucKhenThuong]
GO
ALTER TABLE [dbo].[Table_KhenThuongTapThe]  WITH CHECK ADD  CONSTRAINT [FK_Table_KhenThuongTapThe_Table_PhongBan] FOREIGN KEY([PhongBan_MPB])
REFERENCES [dbo].[Table_PhongBan] ([MaPhongBan])
GO
ALTER TABLE [dbo].[Table_KhenThuongTapThe] CHECK CONSTRAINT [FK_Table_KhenThuongTapThe_Table_PhongBan]
GO
ALTER TABLE [dbo].[Table_KhenThuongTapThe]  WITH CHECK ADD  CONSTRAINT [FK_Table_KhenThuongTapThe_Table_ThoiDiem] FOREIGN KEY([ThoiDiem_MTD])
REFERENCES [dbo].[Table_ThoiDiem] ([MaThoiDiem])
GO
ALTER TABLE [dbo].[Table_KhenThuongTapThe] CHECK CONSTRAINT [FK_Table_KhenThuongTapThe_Table_ThoiDiem]
GO
ALTER TABLE [dbo].[Table_NhanVien]  WITH CHECK ADD  CONSTRAINT [FK_Table_NhanVien_Table_ChucVu] FOREIGN KEY([ChucVu_MCV])
REFERENCES [dbo].[Table_ChucVu] ([MaChucVu])
GO
ALTER TABLE [dbo].[Table_NhanVien] CHECK CONSTRAINT [FK_Table_NhanVien_Table_ChucVu]
GO
ALTER TABLE [dbo].[Table_NhanVien]  WITH CHECK ADD  CONSTRAINT [FK_Table_NhanVien_Table_ChuyenNganh] FOREIGN KEY([ChuyenNganh_MCN])
REFERENCES [dbo].[Table_ChuyenNganh] ([MaChuyenNganh])
GO
ALTER TABLE [dbo].[Table_NhanVien] CHECK CONSTRAINT [FK_Table_NhanVien_Table_ChuyenNganh]
GO
ALTER TABLE [dbo].[Table_NhanVien]  WITH CHECK ADD  CONSTRAINT [FK_Table_NhanVien_Table_PhongBan1] FOREIGN KEY([PhongBan_MPB])
REFERENCES [dbo].[Table_PhongBan] ([MaPhongBan])
GO
ALTER TABLE [dbo].[Table_NhanVien] CHECK CONSTRAINT [FK_Table_NhanVien_Table_PhongBan1]
GO
ALTER TABLE [dbo].[Table_NhanVien]  WITH CHECK ADD  CONSTRAINT [FK_Table_NhanVien_Table_TrinhDoHocVan] FOREIGN KEY([TrinhDo_MTD])
REFERENCES [dbo].[Table_TrinhDoHocVan] ([MaTDHV])
GO
ALTER TABLE [dbo].[Table_NhanVien] CHECK CONSTRAINT [FK_Table_NhanVien_Table_TrinhDoHocVan]
GO
ALTER TABLE [dbo].[Table_SangKienKinhNghiem]  WITH CHECK ADD  CONSTRAINT [FK_Table_SangKienKinhNghiem_Table_NamHoc] FOREIGN KEY([NamHoc_MNH])
REFERENCES [dbo].[Table_NamHoc] ([MaNamHoc])
GO
ALTER TABLE [dbo].[Table_SangKienKinhNghiem] CHECK CONSTRAINT [FK_Table_SangKienKinhNghiem_Table_NamHoc]
GO
ALTER TABLE [dbo].[Table_SangKienKinhNghiem]  WITH CHECK ADD  CONSTRAINT [FK_Table_SangKienKinhNghiem_Table_NhanVien] FOREIGN KEY([NhanVien_MNV])
REFERENCES [dbo].[Table_NhanVien] ([MaNhanVien])
GO
ALTER TABLE [dbo].[Table_SangKienKinhNghiem] CHECK CONSTRAINT [FK_Table_SangKienKinhNghiem_Table_NhanVien]
GO
ALTER TABLE [dbo].[Table_TangLuong]  WITH CHECK ADD  CONSTRAINT [FK_Table_TangLuong_Table_DeNghiTangLuong] FOREIGN KEY([DeNghiTangLuong_MDNTL])
REFERENCES [dbo].[Table_DeNghiTangLuong] ([MaDNTL])
GO
ALTER TABLE [dbo].[Table_TangLuong] CHECK CONSTRAINT [FK_Table_TangLuong_Table_DeNghiTangLuong]
GO
ALTER TABLE [dbo].[Table_TangLuong]  WITH CHECK ADD  CONSTRAINT [FK_Table_TangLuong_Table_NhanVien] FOREIGN KEY([NhanVien_MNV])
REFERENCES [dbo].[Table_NhanVien] ([MaNhanVien])
GO
ALTER TABLE [dbo].[Table_TangLuong] CHECK CONSTRAINT [FK_Table_TangLuong_Table_NhanVien]
GO
ALTER TABLE [dbo].[Table_User]  WITH CHECK ADD  CONSTRAINT [FK_Table_User_Table_NhanVien] FOREIGN KEY([NhanVien_MNV])
REFERENCES [dbo].[Table_NhanVien] ([MaNhanVien])
GO
ALTER TABLE [dbo].[Table_User] CHECK CONSTRAINT [FK_Table_User_Table_NhanVien]
GO
ALTER TABLE [dbo].[Table_VanBanTDKT]  WITH CHECK ADD  CONSTRAINT [FK_Table_VanBanTDKT_Table_LoaiHinh] FOREIGN KEY([LoaiHinh_MLH])
REFERENCES [dbo].[Table_LoaiHinh] ([MaLoaiHinh])
GO
ALTER TABLE [dbo].[Table_VanBanTDKT] CHECK CONSTRAINT [FK_Table_VanBanTDKT_Table_LoaiHinh]
GO
USE [master]
GO
ALTER DATABASE [CSDLQLTDKT_ChinhThuc] SET  READ_WRITE 
GO
